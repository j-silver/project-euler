#include "choose.h"
#include <cmath>
#include <stdexcept>

[[nodiscard]] size_t choose(size_t n, size_t k)
{
    if (k > n) {
        throw std::invalid_argument{"It must be k <= n"};
    }

    double count {1};
    while (k >= 1) {
        count *= static_cast<double>(n)/static_cast<double>(k);
        --n;
        --k;
    }

    return static_cast<size_t>(std::round(count));
}


#ifndef CHOOSE_H
#define CHOOSE_H

#include <cstddef>

[[nodiscard]] size_t choose(size_t n, size_t k);

#endif // CHOOSE_H

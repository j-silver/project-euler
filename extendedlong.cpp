#include "extendedlong.hpp"


Sign operator*(Sign s1, Sign s2)
{
	return (s1 == s2 ? Sign::Plus : Sign::Minus);
}


// helper conversion to vector: digits corresponding to increasing power of 10
std::vector<unsigned> convert_to_vector(long long x) noexcept
{
	std::vector<unsigned> v;
	x = std::abs(x);
	while (x/10 > 0) {
		v.push_back(static_cast<unsigned>(x%10));
		x /= 10;
	}
	v.push_back(static_cast<unsigned>(x));

	return v;
}


// conversion to long long (when possible)
ExtendedLong::operator long long() const
{
	long long L {0};
	if (extlong.size() > std::numeric_limits<long long>::digits10) {
		throw std::overflow_error("Can't be converted to long long");
	}

	unsigned power {0};
	for (const auto& d : extlong) {
		L += static_cast<long long>(d*std::pow(10, power));
		++power;
	}

	return (sgn == Sign::Minus ? -L : L);
}


// helper function: sum of 2 positive ExtendedLong-s
ExtendedLong pos_sum(const ExtendedLong& e1, const ExtendedLong& e2)
{
	auto m {std::min(e1.v().size(), e2.v().size())};
	auto M {std::max(e1.v().size(), e2.v().size())};
	std::vector<unsigned> Sum;
	Sum.reserve(m + M + 1);
	unsigned carry {0};

	for (size_t i {0}; i < m; ++i) {
		auto s {e1.v()[i] + e2.v()[i] + carry};
		Sum.push_back(s%10);
		carry = (s >= 10 ? 1 : 0);
	}

	const auto& largest_e {(e1.v().size() >= e2.v().size() ? e1.v() : e2.v())};

	for (size_t i {m}; i < M; ++i) {
		auto s {largest_e[i] + carry};
		Sum.push_back(s%10);
		carry = (s >= 10 ? 1 : 0);
	}

	if (carry == 1) {
		Sum.push_back(1);
}

	return ExtendedLong {Sum};
}


// Comparison 'less than' between ExtendedLong-s
bool operator<(const ExtendedLong& e1, const ExtendedLong& e2)
{
	bool test {
        [&e1, &e2]()
        {
            if (e1.sign() == Sign::Minus && e2.sign() == Sign::Plus) {
                return true;
            }
            if (e1.sign() == Sign::Plus && e2.sign() == Sign::Minus) {
                return false;
            }
            // they have the same sign. let's assume both positive
            if (e1.v().size() < e2.v().size()) {
                return true;
            }
            if (e1.v().size() > e2.v().size()) {
                return false;
            }
            // same length
            auto e1_last {e1.v().crbegin()};
            auto e2_last {e2.v().crbegin()};
            while (e1_last != e1.v().crend() && *e1_last == *e2_last) {
                --e1_last;
                --e2_last;
            }
            return !(e1_last == e1.v().crend() || *e1_last >= *e2_last);
        }()
	};

	// now we check if they are both positive or negative
    if (e1.sign() == Sign::Minus) { // both negative
        test = !test; // invert the result
    }

    return test;
}


// Comparison 'greater than' between ExtendedLong-s
bool operator>(const ExtendedLong& e1, const ExtendedLong& e2)
{
    bool test {
            [&e1, &e2]()
            {
                if (e1.sign() == Sign::Minus && e2.sign() == Sign::Plus) {
                    return false;
                }
                if (e1.sign() == Sign::Plus && e2.sign() == Sign::Minus) {
                    return true;
                } // they have the same sign. let's assume both positive
                if (e1.v().size() < e2.v().size()) {
                    return false;
                }
                if (e1.v().size() > e2.v().size()) {
                    return true;
                }
                // same number of digits
                auto e1_last {e1.v().crbegin()};
                auto e2_last {e2.v().crbegin()};
                while (e1_last != e1.v().crend() && *e1_last == *e2_last) {
                    --e1_last;
                    --e2_last;
                }
                return !(e1_last == e1.v().crend() || *e1_last <= *e2_last);
            }()
    };

    // now we check if they are both positive or negative
    if (e1.sign() == Sign::Minus) { // both negative
        test = !test; // invert the result
    }

    return test;
}


// Comparison "less or equal" between ExtendedLong-s
bool operator<=(const ExtendedLong& e1, const ExtendedLong& e2)
{
    return !(e1 > e2);
}


// Comparison "greater or equal" between ExtendedLong-s
bool operator>=(const ExtendedLong& e1, const ExtendedLong& e2)
{
    return (!(e1 < e2));
}


// Equality between ExtendedLong-s
bool operator==(const ExtendedLong& e1, const ExtendedLong& e2)
{
    return (!(e1 < e2) && !(e1 > e2));
}


// helper function: difference of 2 positive ExtendedLong-s
ExtendedLong pos_difference(const ExtendedLong& e1, const ExtendedLong& e2)
{
    // assuming e1 >= e2 (this function is not meant to be directly called by
    // the user)
    if (e1 == e2) {
        return ExtendedLong {0};
    }

    std::vector<unsigned> Difference;
    Difference.reserve(e2.v().size());
    int ten {0}; // set to 1 when the e1 digit is < the e2 digit

    auto digit_1 {e1.v().cbegin()};
    for (const auto& digit_2 : e2.v()) {
        int diff {static_cast<int>(*digit_1) -ten -static_cast<int>(digit_2)};
        if (diff < 0) {
            diff = static_cast<int>(*digit_1) -ten +10
                   -static_cast<int>(digit_2);
            ten = 1;
        }
        else {
            ten = 0;
        }
        Difference.push_back(static_cast<unsigned>(diff));
        ++digit_1;
    }

    while (digit_1 != e1.v().end()) {
        int diff {static_cast<int>(*digit_1) - ten};
        if (diff < 0) {
            diff = static_cast<int>(*digit_1) - ten + 10;
            ten = 1;
        }
        else {
            ten = 0;
        }
        Difference.push_back(static_cast<unsigned>(diff));
        ++digit_1;
    }

    while (Difference.back() == 0 && Difference.size() > 1) {
        Difference.pop_back();
    }

    return ExtendedLong {Difference};
}


// Output operator to ostream
std::ostream& operator<<(std::ostream& os, const ExtendedLong& EL)
{
    if (EL.sgn == Sign::Minus) {
        os << "-";
    }

    for (auto digit {EL.v().crbegin()}; digit != EL.v().crend(); ++digit) {
        os << *digit;
    }

    return os;
}


// Returns the opposite (doesn't change the original)
ExtendedLong ExtendedLong::operator-() const
{
    if (this->is_zero()) {
        return *this;
    }

    ExtendedLong opposite {*this};
    opposite.sgn = (this->sign() == Sign::Plus ? Sign::Minus : Sign::Plus);
    return opposite;
}


// algebraic sum of ExtendedLong-s
ExtendedLong operator+(const ExtendedLong& e1, const ExtendedLong& e2)
{
    ExtendedLong Sum;

    if (e1.sign() == e2.sign()) {
        Sum = pos_sum(e1, e2);
        if (e1.sign() == Sign::Minus) {
            Sum = -Sum;
        }
    }
    else {
        const auto e1_abs = (e1.sign() == Sign::Plus ? e1 : -e1);
        const auto e2_abs = (e2.sign() == Sign::Plus ? e2 : -e2);
        if (e1_abs >= e2_abs) {
            Sum = pos_difference(e1_abs, e2_abs);
        } else {
            Sum = pos_difference(e2_abs, e1_abs);
            Sum = -Sum;
        }
    }

    return Sum;
}


// algebraic difference of ExtendedLong-s
ExtendedLong operator-(const ExtendedLong& e1, const ExtendedLong& e2)
{
    return {e1 + (-e2)};
}


// sum of ExtendedLong and long long
ExtendedLong operator+(const ExtendedLong& e, long long l)
{
    const auto l_ext = (l >= 0 ? ExtendedLong {l} : - ExtendedLong {l});
	return (e + l_ext);
}


ExtendedLong operator+(long long l, const ExtendedLong& e)
{
	return (e + l);
}


ExtendedLong operator-(const ExtendedLong& e, long long l)
{
	const auto l_ext = (l >= 0 ? ExtendedLong {l} : - ExtendedLong {l});
	return (e - l_ext);
}


ExtendedLong operator-(long long l, const ExtendedLong& e)
{
	return (e - l);
}


ExtendedLong& ExtendedLong::operator+=(const ExtendedLong& e)
{
	*this = *this + e;
	return *this;
}


ExtendedLong& ExtendedLong::operator-=(const ExtendedLong& e)
{
	*this = *this - e;
	return *this;
}


ExtendedLong& ExtendedLong::operator+=(long long l)
{
	*this = *this + l;
	return *this;
}


ExtendedLong& ExtendedLong::operator-=(long long l)
{
	*this = *this - l;
	return *this;
}


ExtendedLong& ExtendedLong::operator++()
{
	return (this->operator+=(1));
}


ExtendedLong& ExtendedLong::operator--()
{
	return (this->operator-=(1));
}


ExtendedLong ExtendedLong::operator++(int)
{
    ExtendedLong notIncremented {*this};
	operator++();
	return notIncremented;
}


ExtendedLong ExtendedLong::operator--(int)
{
    ExtendedLong notDecremented {*this};
    operator--();
	return notDecremented;
}


ExtendedLong operator*(const ExtendedLong& e, long long l)
{
	ExtendedLong product {0};

	if (l == 0 || e.is_zero()) {
		return product;
    }
	
	return e * ExtendedLong {l};
}


ExtendedLong operator*(long long l, const ExtendedLong& e)
{
	return e*l;
}


long long operator*(const std::vector<unsigned>& v1,
					const std::vector<unsigned>& v2)
{
	if (v1.size()+v2.size() >= std::numeric_limits<long long>::digits10) {
		throw std::overflow_error("product of vectors is too long.");
	}
	
	if (v1.empty() || v2.empty()) {
		return 0;
    }

	long long factor_1 {0};
	long long pow_of_ten {1};

	for (auto digit {v1.cbegin()}; digit != v1.cend(); ++digit) {
		factor_1 += *digit * pow_of_ten;
		pow_of_ten *= 10;
	}

	long long factor_2 {0};
	pow_of_ten = 1;
	for (auto digit {v2.cbegin()}; digit != v2.cend(); ++digit) {
		factor_2 += *digit * pow_of_ten;
		pow_of_ten *= 10;
	}

	return factor_1 * factor_2;
}


ExtendedLong ten_to(size_t m, ExtendedLong const& e)
{
	if (m == 0) {
		return e;
    }

	std::vector<unsigned> vec (m + e.v().size());
	auto end {std::next(vec.begin(), static_cast<long>(m))};
	std::fill(vec.begin(), end, 0);
	std::copy(e.v().cbegin(), e.v().cend(), end);

	return ExtendedLong {vec};
}


std::pair<std::vector<unsigned>, std::vector<unsigned>>	split(const std::vector<unsigned>& v)
{
	auto const half_l {static_cast<std::vector<unsigned>::difference_type>(v.size()/2)};
	std::vector<unsigned> v_low {v.begin(), v.begin() + half_l};
	std::vector<unsigned> v_high {v.begin() + half_l, v.end()};

	while (v_low.back() == 0 && !v_low.empty()) {
	    v_low.pop_back();
    }
	while (v_high.back() == 0 && !v_high.empty()) {
	    v_high.pop_back();
    }

	return {v_low, v_high};
}


ExtendedLong karatsuba(const std::vector<unsigned>& v1,
                       const std::vector<unsigned>& v2)
{
	size_t const sz1 {v1.size()};
	size_t const sz2 {v2.size()};
	if (sz1 == 0 || sz2 == 0) {
		return ExtendedLong {0};
    }

	ExtendedLong karatsuba_product;

	if (sz1 + sz2 < max_length) {
		karatsuba_product = ExtendedLong {v1*v2};
	} else {
		size_t const m1 {v1.size()/2};
		size_t const m2 {v2.size()/2};
		const auto [v1_low, v1_high] = split(v1);
		const auto [v2_low, v2_high] = split(v2);
		ExtendedLong low {karatsuba(v1_low, v2_low)};
		ExtendedLong mid2 {karatsuba(v2_high, v1_low)};
		ExtendedLong mid1 {karatsuba(v1_high, v2_low)};
		ExtendedLong high {karatsuba(v1_high, v2_high)};
		karatsuba_product = low	+ ten_to(m2, mid2) + ten_to(m1, mid1)
								+ ten_to(m1+m2, high);
	}
	
	return karatsuba_product;
}


ExtendedLong operator*(const ExtendedLong& e1, const ExtendedLong& e2)
{
	const auto karatsuba_product {karatsuba(e1.v(), e2.v())};
	return ExtendedLong {karatsuba_product.v(), e1.sign()*e2.sign()};
}


ExtendedLong& ExtendedLong::operator*=(long long n)
{
	*this = *this * n;
	return *this;
}


ExtendedLong operator^(const ExtendedLong& e, long long n)
{
	ExtendedLong x {1};
	while (n > 0) {
		x = x*e;
		--n;
	}
	return x;
}

#include <vector>
#include <cmath>
#include <iostream>
#include <limits>
#include <stdexcept>

constexpr auto max_length
			{static_cast<size_t>(std::numeric_limits<long long>::digits10)};

enum class Sign { Plus, Minus };

Sign operator*(Sign s1, Sign s2);

std::vector<unsigned> convert_to_vector(long long x) noexcept;


class ExtendedLong {
private:
	std::vector<unsigned> extlong;
	Sign sgn;

public:
	// constructors
	explicit ExtendedLong(long long e = 0) noexcept:
		extlong {convert_to_vector(e)},
		sgn {(e >= 0 ? Sign::Plus : Sign::Minus)} {};

	explicit ExtendedLong(const std::vector<unsigned>& v, Sign s = Sign::Plus) noexcept:
		extlong {v}, sgn {s} {};


	const std::vector<unsigned>& v() const noexcept { return extlong; }
	Sign sign() const noexcept { return sgn; } 

	// conversion
	explicit operator long long() const;

	bool is_zero() const noexcept
	{
		if (extlong.size() == 1 && extlong[0] == 0)
			return true;
		else
			return false;
	}

	bool is_even() const noexcept { return extlong[0]%2 == 0 ? true : false; };

	ExtendedLong operator-() const;

	// friend operators
	friend std::ostream& operator<<(std::ostream&, const ExtendedLong&);

	friend bool operator<(const ExtendedLong& e1, const ExtendedLong& e2);
	friend bool operator>(const ExtendedLong& e1, const ExtendedLong& e2);
	friend bool operator<=(const ExtendedLong& e1, const ExtendedLong& e2);
	friend bool operator>=(const ExtendedLong& e1, const ExtendedLong& e2);
	friend bool operator==(const ExtendedLong& e1, const ExtendedLong& e2);

	ExtendedLong& operator+=(const ExtendedLong& e);
	ExtendedLong& operator+=(long long l);
	ExtendedLong& operator-=(const ExtendedLong& e);
	ExtendedLong& operator-=(long long l);
	ExtendedLong& operator++();
	ExtendedLong& operator--();
	ExtendedLong operator++(int);
	ExtendedLong operator--(int);
	ExtendedLong& operator*=(long long n);

protected:
	// friend helper functions
	friend ExtendedLong pos_sum(const ExtendedLong& e1, const ExtendedLong& e2);
	friend ExtendedLong pos_difference(const ExtendedLong& e1,
										const ExtendedLong& e2);
};


// helper conversion to vector: digits corresponding to increasing power of 10
std::vector<unsigned> convert_to_vector(long long x) noexcept;


// helper function: sum of 2 positive ExtendedLong-s
ExtendedLong pos_sum(const ExtendedLong& e1, const ExtendedLong& e2);


// Comparison 'less than' between ExtendedLong-s
bool operator<(const ExtendedLong& e1, const ExtendedLong& e2);


// Comparison 'greater than' between ExtendedLong-s
bool operator>(const ExtendedLong& e1, const ExtendedLong& e2);


// Comparison "less or equal" between ExtendedLong-s
bool operator<=(const ExtendedLong& e1, const ExtendedLong& e2);



// Comparison "greater or equal" between ExtendedLong-s
bool operator>=(const ExtendedLong& e1, const ExtendedLong& e2);


// Equality between ExtendedLong-s
bool operator==(const ExtendedLong& e1, const ExtendedLong& e2);


// helper function: difference of 2 positive ExtendedLong-s
ExtendedLong pos_difference(const ExtendedLong& e1, const ExtendedLong& e2);


// Output operator to ostream
std::ostream& operator<<(std::ostream& os, const ExtendedLong& EL);


// algebraic sum of ExtendedLong-s
ExtendedLong operator+(const ExtendedLong& e1, const ExtendedLong& e2);


// algebraic difference of ExtendedLong-s
ExtendedLong operator-(const ExtendedLong& e1, const ExtendedLong& e2);


// sum of ExtendedLong and long long
ExtendedLong operator+(const ExtendedLong& e, long long l);
ExtendedLong operator+(long long l, const ExtendedLong& e);


// algebraic difference of ExtendedLong and long long
ExtendedLong operator-(const ExtendedLong& e, long long l);
ExtendedLong operator-(long long l, const ExtendedLong& e);

ExtendedLong operator*(const ExtendedLong& e, long long l);
ExtendedLong operator*(long long l, const ExtendedLong& e);

ExtendedLong operator*(const ExtendedLong& e1, const ExtendedLong& e2);

ExtendedLong operator^(const ExtendedLong& e, long long n);


#include "primes.hpp"
#include <cmath>
#include <stdexcept>
#include <algorithm>

shared_ptr<vector<Ull>>
find_primes_up_to(Ull sup)
{
    auto primes {make_shared<vector<Ull>> (initializer_list<Ull>{2})};

    // Use Prime Number Theorem to estimate how much space we need to reserve
    primes->reserve(sup/static_cast<size_t>(log(sup)));

    for (Ull candidate {3}; candidate <= sup; ++candidate) {
        bool isPrime {true};
        for (const auto& prime_div: *primes) {
            if (prime_div*prime_div <= candidate && candidate%prime_div != 0) {
                continue;
            }
            isPrime = candidate%prime_div != 0;
            break;
        }
        if (isPrime) {
            primes->push_back(candidate);
        }
    }

    return primes;
}

// Finds primes between inf and sup. pr_pt must be a shared_ptr to a vector
// of primes starting from 2
vector<Ull>
find_primes_in_range(Ull inf, Ull sup,
                     shared_ptr<vector<Ull> const> const& pr_pt)
{
    if (inf >= sup) {
        throw domain_error("inf >= sup");
    }
    
    // Vector of primes from inf to sup. Allocate space
    vector<Ull> primes;
    primes.reserve((sup-inf)/static_cast<Ull>(log(sup - inf)));
    
    // Don't check the already known primes
    auto start {lower_bound(pr_pt->cbegin(), pr_pt->cend(), inf)};
    if (start != pr_pt->cend()) {
        for (auto& p {start}; p != pr_pt->cend(); ++p) {
            primes.push_back(*p);
        }
    }

    for (Ull candidate {inf}; candidate <= sup; ++candidate) {
        bool isPrime {true};
        for (const auto& prime_div: *pr_pt) {
            if (candidate%prime_div == 0 && prime_div*prime_div <= candidate) {
                // candidate is not a prime
                isPrime = false;
                break;
            }
            if (prime_div*prime_div > candidate) {
                // candidate is a prime
                isPrime = true;
            }
            else {
                // we need proceeding until divisor^2 > candidate
                auto divisor {prime_div + 1};
                while (divisor*divisor <= candidate && candidate%divisor != 0) {
                    ++divisor;
                }
                isPrime = candidate%divisor != 0;
                break;
            }
        }
        if (isPrime) {
            primes.push_back(candidate);
        }
    }

    return primes;
}

// Checks if it's a prime number using the given known primes
[[maybe_unused]] bool is_prime(Ull candidate, shared_ptr<vector<Ull> const> const& primes)
{
    if (candidate == 1) {
        return true;
    }

    auto search_in {
            [candidate](auto const& x)
            { return x > 1 && x <= static_cast<Ull>(sqrt(candidate)) && candidate % x == 0; }
    };
    
    if (primes->back() <= static_cast<Ull>(sqrt(candidate))) {
        auto newPrimes {
                find_primes_in_range(primes->back() + 1,
                                     static_cast<Ull>(sqrt(candidate)),
                                     primes)
        };
        if (std::any_of(newPrimes.cbegin(), newPrimes.cend(), search_in)) {
            return false;
        }
    }

    return !(std::any_of(primes->cbegin(), primes->cend(), search_in));
}

bool is_prime(int candidate, shared_ptr<vector<Ull> const> const& primes)
{
    return is_prime(static_cast<Ull>(candidate), primes);
}

// build a vector<bool> 'numbers' where numbers[n] = true if n is prime
shared_ptr<vector<bool>> which_are_primes(const Ull sup)
{
    auto numbers {make_shared<vector<bool>>(vector<bool> (sup+1, true))};

    for (size_t index {2}; index < numbers->size(); ++index)
    {
        if (!(*numbers)[index]) {
            continue;
        }
        size_t multiplier {2};
        while (multiplier*index <= sup) {
            (*numbers)[multiplier*index] = false;
            ++multiplier;
        }
    }

    return numbers;
}
#include <vector>
#include <memory>

using namespace std;

using Ull = unsigned long long;


// Find all primes up to 'sup', return a shared_ptr to a vector of
// unsigned long long
shared_ptr<vector<Ull>> find_primes_up_to(Ull sup);


// Finds primes between inf and sup. pr_pt must be a shared_ptr to a vector of
// unsigned long long primes starting from 2
vector<Ull> find_primes_in_range(Ull inf, Ull sup,
                                 shared_ptr<vector<Ull> const> const& pr_pt);

// Checks if candidate is a prime number, given a vector of primes
bool is_prime(Ull candidate, shared_ptr<vector<Ull> const> const& primes);

bool is_prime(int candidate, shared_ptr<vector<Ull> const> const& primes);


// build a vector<bool> 'numbers' where numbers[n] = true if n is prime
shared_ptr<vector<bool>> which_are_primes(Ull sup);

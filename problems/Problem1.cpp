#include <iostream>
#include <numeric>
#include <set>
#include <cassert>

int main()
{
    constexpr int fiveMultiplesBound {1000 / 5};
    constexpr int threeMultiplesBound {1000 / 3};

    std::set<int> multiples;

    for (int n {1}; n <= fiveMultiplesBound; ++n) {
        multiples.emplace(n * 5);
    }
    for (int n {1}; n <= threeMultiplesBound; ++n) {
        multiples.emplace(n * 3);
    }

    if (auto thousand {multiples.find(1000)}; thousand != multiples.end()) {
        multiples.erase(thousand);
    }

    auto const answer {std::accumulate(multiples.cbegin(), multiples.cend(), 0)};
    assert(answer == 233168);
    std::cout << answer << '\n';
}

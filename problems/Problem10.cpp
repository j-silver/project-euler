//
// Created by giuseppe on 24/11/2020.
//

#include <iostream>
#include <numeric>
#include <cassert>
#include "../primes.hpp"

int main()
{
    auto const primesUpTo2M {find_primes_up_to(2'000'000)};
    auto const sum {std::accumulate(primesUpTo2M->cbegin(), primesUpTo2M->cend(), 0ULL)};
    assert(sum == 142913828922);
    std::cout << sum << '\n';
}

#include <iostream>
#include "Problem100.h"


[[nodiscard]] double solution(double n)
{
    return 0.5*(1 + std::sqrt(1 + 2*n*n - 2*n));
}


bool lhs_is_less_than_rhs(Ull b, Ull n)
{
    auto doubleB {static_cast<double>(b)};
    auto doubleN {static_cast<double>(n)};
    double product {doubleB / doubleN};
    product *= (doubleB - 1.0);
    product /= (doubleN - 1.0);
    return product < 0.5;
}


bool lhs_is_less_than_rhs(double b, double n)
{
    auto product {b/n};
    product *= (b-1.0);
    product /= (n-1.0);
    return product < 0.5;
}


bool intersect(Ull b, Ull n)
{
    auto B {static_cast<double>(b)};
    double rhs {static_cast<double>(n%2 == 0 ? n/2 : (n - 1)/2)};
    double lhs {n%2 == 0 ? B/static_cast<double>(n - 1) :
                           B/static_cast<double>(n)};
    lhs *= (B - 1.0);

    return std::abs(lhs-rhs) <= epsilon * std::abs(rhs+lhs) * 1
           // unless the result is subnormal
           || std::abs(lhs-rhs) < minDouble;
}


bool intersect(double b, double n)
{
    auto rhs {n/2.0};
    auto lhs {b/(n-1.0)};
    lhs *= (b-1.0);

    return std::abs(lhs-rhs) <= epsilon * std::abs(rhs+lhs) * 1
           // unless the result is subnormal
           || std::abs(lhs-rhs) < minDouble;
}


Ull find_blue(Ull startUpperLimit)
{
    auto startDouble {upper_limit(startUpperLimit)};
    auto start {static_cast<Ull> (std::floor(startDouble))};
    for (Ull N {start + 1}; N < std::numeric_limits<Ull>::max() - 1; ++N) {
        for (Ull B {startUpperLimit};
             ! lhs_is_less_than_rhs(B, N);
             --B)
        {
            if (intersect(B, N))
                return B;
        }
    }

    return 0;
}


double find_blue_double(Ull startUpperLimit)
{
    auto startDouble {upper_limit(startUpperLimit)};
    auto start {std::floor(startDouble)};

    for (double N {start + 1.0}; N < Max; N += 1.0)
    {
        for (double B {static_cast<double>(startUpperLimit)};
             ! lhs_is_less_than_rhs(B, N);
             B -= 1.0)
        {
            if (intersect(B, N))
                return B;
        }
    }

    return 0;
}




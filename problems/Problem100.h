//
// Created by giuseppe on 20/09/2019.
//

#ifndef PROJECTEULER_PROBLEM100_H
#define PROJECTEULER_PROBLEM100_H

#include <limits>
#include <cmath>

using Ull = unsigned long long;

constexpr double epsilon {std::numeric_limits<double>::epsilon()};
constexpr double minDouble {std::numeric_limits<double>::min()};
constexpr double Max {10e13};


constexpr double upper_limit(Ull n)
{
    return (1.0 + std::sqrt(2)*static_cast<double> (n))/2.0;
}

[[nodiscard]] double solution(double n);
bool lhs_is_less_than_rhs(Ull b, Ull n);
bool lhs_is_less_than_rhs(double b, double n);

bool intersect(Ull b, Ull n);
bool intersect(double b, double n);

Ull find_blue(Ull startUpperLimit);
double find_blue_double(Ull startUpperLimit);


#endif //PROJECTEULER_PROBLEM100_H

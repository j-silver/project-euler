//
// Created by giuseppe on 20/09/2019.
//


#include "Problem100.h"
#include <gtest/gtest.h>

constexpr Ull nStart {4'050};


TEST(Problem100_test, lhs_is_less_than_rhs)
{
    for (Ull N {1}; N < 10'000; ++N) {
        for (Ull blues {1}; blues < N; ++blues) {
            auto B {static_cast<double> (blues)};
            auto Ndouble {static_cast<double> (N)};
            if (B*(B - 1)/Ndouble/(Ndouble - 1) >= 0.5)
                GTEST_CHECK_(! lhs_is_less_than_rhs(blues, N));
            else
                continue;
        }
    }
}


TEST(Problem100_test, intersect)
{
    std::map<Ull, Ull> intersections;

    for (Ull N {3}; N < 10'000; ++N) {
        for (Ull blues {1}; blues < N; ++blues) {
            if (intersect(blues, N)) {
                intersections[N] = blues;
            }
        }
    }
    std::cerr << "             Counted intersections: " << intersections.size() << '\n';
    for (auto const& i : intersections) {
        std::cerr << i.second << " blue balls in " << i.first << " balls.\n";
    }
}


TEST(Problem100_test, find_blue)
{
    auto result {find_blue_double(nStart)};
    GTEST_ASSERT_EQ(result, 2871);
}


TEST(Problem100_test, doubles)
{
    std::cerr << "epsilon = " << epsilon << '\n';
    std::cerr << "minimum double = "  << minDouble << '\n';
 
    for (double n {10e12}; n < Max; n += 1.0) {
        double b {solution(n)};
        if (auto difference {abs(b  - std::round(b))};
            difference < epsilon*2*std::ceil(b) || difference < minDouble)
        {
            std::cerr << "difference: " << b - std::round(b) << '\n';
            std::cerr << "2*epsilon*b: " << 2*epsilon*b << '\n';
            auto oldPrecision {std::cerr.precision(14)};
            std::cerr << b << " blue balls in a group of " << n << " balls\n";
            std::cerr.precision(oldPrecision);
            break;
        }
    }
}


int main()
{
    ::testing::InitGoogleTest();
    auto returnValue {RUN_ALL_TESTS()};
    return returnValue;
}

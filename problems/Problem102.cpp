//
// Created by giuseppe on 02/10/2019.
//

#include <tuple>
#include <algorithm>
#include "Problem102.h"

using namespace std::complex_literals;



template<typename T>
typename std::enable_if_t<!std::numeric_limits<T>::is_integer, bool>
almost_equal(T x, T y, int ulp)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return // std::abs(x-y) <= std::numeric_limits<T>::epsilon()*(std::abs(x) + std::abs(y))*ulp ||
           // unless the result is subnormal
           //std::abs(x-y) < std::numeric_limits<T>::min();
            std::abs(x-y) < 1e-10 * ulp;
}


bool operator==(Point const& p1, Point const& p2)
{
    return almost_equal(p1.x(), p2.x(), 1) &&
           almost_equal(p1.y(), p2.y(), 1);
}

Point operator+(Point const& p1, Point const& p2)
{
    return Point{p1.x() + p2.x(), p1.y() + p2.y()};
}

Point operator-(Point const& p1, Point const& p2)
{
    return Point{p1.x() - p2.x(), p1.y() - p2.y()};
}


Triangle::Triangle(Point const& p1, Point const& p2, Point const& p3) :
        _vertices {p1, p2, p3}
{
    if (p1 == p2 || p1 == p3 || p2 == p3)
        throw std::invalid_argument{"Points must be distinct"};
}


Point& Point::rotate_to(Point a)
{
    auto phase {-std::arg(std::complex<double>(a)) * 1i};
    auto rotated {std::complex<double>(*this) * exp(phase)};
    *this = rotated;
    return *this;
}


bool Triangle::contains(Point p)
{
    if (std::any_of(_vertices.cbegin(), _vertices.cend(),
                    [p](auto const& v) { return v == p; }))
        return true;

    auto rotatedAC {(C()-A()).rotate_to(B()-A())};
    auto ap  {p - A()};
    ap.rotate_to(B()-A());

    auto angle {std::arg(std::complex<double>(rotatedAC))};
    auto pAngle {std::arg(std::complex<double>(ap))};

    if ((angle > 0 && pAngle < 0) || (angle < 0 && pAngle > 0))
        return false;
    if (angle > 0 && pAngle > angle)
        return false;
    if (angle < 0 && pAngle < angle)
        return false;

    auto rotatedBA {(A()-B()).rotate_to(C()-B())};
    auto bp {p - B()};
    bp.rotate_to(C()-B());

    angle = std::arg(std::complex<double>(rotatedBA));
    pAngle = std::arg(std::complex<double>(bp));

    if ((angle > 0 && pAngle < 0) || (angle < 0 && pAngle > 0))
        return false;
    if (angle > 0 && pAngle > angle)
        return false;
    if (angle < 0 && pAngle < angle)
        return false;

    return true;
}


#ifndef PROBLEM102_H
#define PROBLEM102_H

#include <vector>
#include <complex>


const double PI {acos(-1)};

class Point {
public:
    Point(double x, double y) : _x {x}, _y {y} {}
    Point() : _x{0}, _y{0} {}

    Point(std::complex<double> z) : _x {real(z)}, _y {imag(z)} {}
    Point& operator=(Point const& p) = default;

    [[nodiscard]] double x() const { return _x; }
    [[nodiscard]] double y() const { return _y; }

    operator std::complex<double>() const { return std::complex<double>(_x, _y); }

    Point& rotate_to(Point a);

private:
    double _x;
    double _y;
};

bool operator==(Point const& p1, Point const& p2);

Point operator+(Point const& p1, Point const& p2);
Point operator-(Point const& p1, Point const& p2);


class Triangle {
public:
    Triangle(Point const& p1, Point const& p2, Point const& p3);
    explicit Triangle(std::vector<Point>&& v)
    : _vertices {std::forward<std::vector<Point>>(v)} {}

    [[nodiscard]] Point A() const { return _vertices[0]; }
    [[nodiscard]] Point B() const { return _vertices[1]; }
    [[nodiscard]] Point C() const { return _vertices[2]; }
    [[nodiscard]] std::vector<Point> const& vertices() const { return _vertices; }

    bool contains(Point p);
private:
    std::vector<Point> _vertices;
};





#endif // PROBLEM102_H
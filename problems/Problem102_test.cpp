//
// Created by giuseppe on 02/10/2019.
//

#include "Problem102.h"

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>


class Points : public ::testing::Test {
public:
    Points()
    :
    point1 {-1, 1},
    point2 {-1, 0},
    point3 {0, 1},
    point4 {1, 3},
    point5 {1, 3},
    point6 {0, -5}
    {}

    Point point1;
    Point point2;
    Point point3;
    Point point4;
    Point point5;
    Point point6;

private:
    void TestBody() override {}
};


class Triangles : public ::testing::Test {
public:
    Triangles()
    :
    pts {},
    T {pts.point3, pts.point2, pts.point1},
    U {pts.point1, pts.point3, pts.point5},
    V {pts.point6, pts.point2, pts.point4}
    {}

    Points pts;
    Triangle T;
    Triangle U;
    Triangle V;
};


TEST_F(Points, points_validity)
{
    GTEST_ASSERT_EQ(point4, point5);

    auto point7 {point3};
    GTEST_ASSERT_EQ(point7, point3);

    GTEST_ASSERT_EQ(point1-point2, (Point{0, 1}));
    GTEST_ASSERT_EQ(point1+point2, (Point{-2, 1}));

    point3.rotate_to(point3);
    GTEST_CHECK_(point3 == (Point{1, 0}));
}



TEST_F(Triangles, distinct_points)
{
   GTEST_TEST_THROW_((Triangle {{1, 0}, {1, 0}, {0, 0}}),
                     std::invalid_argument,
                     );
}


TEST_F(Triangles, contains_origin)
{
    Triangle test {{-1, -1}, {1, -1}, {0, 1}};
    GTEST_CHECK_(test.contains({0,0}));

    test = Triangle {{0, 1}, {1, 1}, {1, 0}};
    GTEST_CHECK_(! test.contains({0,0}));

    test = Triangle {{0, 0}, {0, 1}, {1, 1}};
    GTEST_CHECK_(test.contains({0, 0}));
    GTEST_CHECK_(test.contains({0.5, 0.75}));
    GTEST_CHECK_(! test.contains({-1, -1}));
    GTEST_CHECK_(test.contains({0.5, 0.5}));

    Triangle a {{-340, 495}, {-153, -910}, {835,-947}};
    Triangle b {{-175, 41}, {-421, -714}, {574, -645}};
    GTEST_CHECK_(a.contains({0, 0}));
    GTEST_CHECK_(! b.contains({0,0}));
}


TEST(Problem102, problem)
{
    int count {0};
    std::ifstream fileStream {"../problems/p102_triangles.txt"};
    if (! fileStream.good())
        throw std::exception {};

    std::string line;
    double x, y;

    while (std::getline(fileStream, line)) {
        std::vector<Point> points;
        std::istringstream input {line};
        for (int i {0}; i < 3; ++i) {
            input >> x;
            input.get();
            input >> y;
            input.get();
            points.emplace_back(Point{x, y});
        }
        Triangle t {std::move(points)};
        if (t.contains({0, 0}))
            ++count;
    }

    std::cout << count << '\n';
}


int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


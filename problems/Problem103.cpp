//
// Created by giuseppe on 01/01/2020.
//

#include <numeric>
#include "Problem103.h"
#include "../subsets.h"


template<std::forward_iterator Fwd>
int sum_of_subset(std::vector<Fwd> const& s)
{
    return std::accumulate(s.cbegin(), s.cend(), 0,
                           [](int init, auto const& it)
                           {
                                return init + *it;
                           });
}


bool is_special(std::set<int> const& setOfNumbers)
{
    Subsets subsets {cbegin(setOfNumbers), cend(setOfNumbers)};
    for (size_t sz {1}; sz <= setOfNumbers.size()-1; ++sz) {
        for (std::set<int> sums; auto const& sub : subsets.build(sz)) {
            auto sum {sum_of_subset(sub)};
            if ( sums.contains(sum_of_subset(sub))) {
                return false;
            }
            sums.insert(sum);
        }
    }

    auto const totalSum {std::accumulate(setOfNumbers.cbegin(), setOfNumbers.cend(), 0)};
    auto const halfSize {static_cast<long>(setOfNumbers.size()/2 + 1)};
    for (auto start {setOfNumbers.begin()};
         std::distance(start, setOfNumbers.end()) > halfSize;
         ++start)
    {
        if (auto sum {std::accumulate(start, std::next(start, halfSize), 0,
                                      [](int init, auto const& n)
                                      { return init + n; })};
            sum <= totalSum - sum)
        {
            return false;
        }
    }

    return true;
}
 #include "../subsets.cpp"

//
// Created by giuseppe on 01/01/2020.
//

#ifndef PROJECTEULER_PROBLEM103_H
#define PROJECTEULER_PROBLEM103_H

#include <set>


bool is_special(std::set<int> const& setOfNumbers);


#endif //PROJECTEULER_PROBLEM103_H

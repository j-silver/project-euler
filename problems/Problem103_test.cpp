//
// Created by giuseppe on 01/01/2020.
//

#include "Problem103.h"
#include "../subsets.h"
#include "../choose.h"

#include <gtest/gtest.h>


void fail(auto arg) { std::cerr << "failed: " << arg << '\n'; }

using It = std::set<int>::const_iterator;


TEST(choose, test_choose_function)
{
    GTEST_ASSERT_EQ(choose(3, 1), 3);
    GTEST_ASSERT_EQ(choose(3, 3), 1);
    GTEST_ASSERT_EQ(choose(3, 2), 3);
    GTEST_ASSERT_EQ(choose(5, 2), 10);
    GTEST_ASSERT_EQ(choose(5, 3), 10);
    GTEST_ASSERT_EQ(choose(5, 0), 1);
}


TEST(subsets, test_cardinality)
{
    std::set setOfNumbers {1, 2, 3, 4, 5};
    Subsets subsets {cbegin(setOfNumbers), cend(setOfNumbers)};
    auto choose2 {subsets.build(2)};

    GTEST_ASSERT_EQ(choose2.size(), choose(5, 2));
}


TEST(subsets, test_special)
{
    std::set set1 {2, 3, 4};
    std::set set2 {3, 5, 6, 7};
    std::set set3 {6, 9, 11, 12, 13};
    std::set set4 {2, 3, 4, 5};
    GTEST_CHECK_(is_special(set1));
    GTEST_CHECK_(is_special(set2));
    GTEST_CHECK_(is_special(set3));
    GTEST_CHECK_(! is_special(set4));
}


TEST(subsets, test_subsets_from_range)
{
    std::set set1 {2, 3, 4};

    GTEST_TEST_NO_THROW_(auto subsRng {subsets_from_range(set1)}, fail);
    GTEST_TEST_NO_THROW_(Subsets subsRng {set1}, fail);
}


TEST(subsets, test_range_constructor_validity)
{
    std::set set1 {2, 3, 4};
    Subsets subsRng {set1};
    Subsets subsIt {cbegin(set1), cend(set1)};
    for (size_t k {1}; k <= set1.size()-1; ++k) {
        auto chooseIt {subsIt.build(k)};
        auto chooseRng {subsRng.build(k)};
        for (size_t i {1}; i < chooseIt.size(); ++i)
            GTEST_ASSERT_EQ(chooseIt[i], chooseRng[i]);
    }
}


int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#include "../subsets.cpp"

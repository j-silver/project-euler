//
// Created by giuseppe on 24/11/2020.
//

#include <valarray>
#include <filesystem>
#include <fstream>
#include <numeric>
#include <iostream>
#include <future>
#include <cassert>
#include <thread>
#include <iterator>
#include <vector>

unsigned long max_horizontal_product(std::valarray<unsigned long> const& grid)
{
    unsigned long currentMax {0};
    size_t row {0};
    for (int column {0}; row <= 19; ++column) {
        std::slice const s {row*20 + column, 4, 1};
        std::valarray<unsigned long> const strip {grid[s]};
        currentMax = std::max(currentMax, std::accumulate(std::begin(strip), std::end(strip), 1UL, std::multiplies{}));
        if (column == 16) {
            ++row;
            column = -1;
        }
    }
    return currentMax;
}

unsigned long max_vertical_product(std::valarray<unsigned long> const& grid)
{
    unsigned long currentMax {0};
    size_t row {0};
    for (int column {0}; row <= 16; ++column) {
        std::slice const s {row*20 + column, 4, 20};
        std::valarray<unsigned long> const strip {grid[s]};
        currentMax = std::max(currentMax, std::accumulate(std::begin(strip), std::end(strip), 1UL, std::multiplies{}));
        if (column == 19) {
            ++row;
            column = -1;
        }
    }
    return currentMax;
}

unsigned long max_down_diagonal_product(std::valarray<unsigned long> const& grid)
{
    unsigned long currentMax {0};
    size_t row {0};
    for (int column {0}; row <= 16; ++column) {
        std::slice const s {row*20 + column, 4, 21};
        std::valarray<unsigned long> const strip {grid[s]};
        currentMax = std::max(currentMax, std::accumulate(std::begin(strip), std::end(strip), 1UL, std::multiplies{}));
        if (column == 16) {
            ++row;
            column = -1;
        }
    }
    return currentMax;
}

unsigned long max_up_diagonal_product(std::valarray<unsigned long> const& grid)
{
    unsigned long currentMax {0};
    size_t row {0};
    for (int column {3}; row <= 16; ++column) {
        std::slice const s {row*20 + column, 4, 19};
        std::valarray<unsigned long> const strip {grid[s]};
        currentMax = std::max(currentMax, std::accumulate(std::begin(strip), std::end(strip), 1UL, std::multiplies{}));
        if (column == 16) {
            ++row;
            column = 2;
        }
    }
    return currentMax;
}

int main()
{
    std::ifstream fileStream {std::filesystem::path {"../problems/problem10.txt"}};
    std::valarray<unsigned long> grid (20*20);
    std::copy(std::istream_iterator<unsigned long>{fileStream}, std::istream_iterator<unsigned long>{},
              std::begin(grid));

    std::vector<std::packaged_task<unsigned long(std::valarray<unsigned long> const&)>> tasks;
    tasks.reserve(4);
    tasks.emplace_back(&max_horizontal_product);
    tasks.emplace_back(&max_vertical_product);
    tasks.emplace_back(&max_down_diagonal_product);
    tasks.emplace_back(&max_up_diagonal_product);

    std::vector<std::future<unsigned long>> futures;
    futures.reserve(4);
    std::transform(tasks.begin(), tasks.end(), std::back_inserter(futures), [](auto& f){ return f.get_future(); });

    std::vector<std::jthread> threads;
    threads.reserve(4);
    std::transform(tasks.begin(), tasks.end(), std::back_inserter(threads),
                   [&grid](auto&& t){ return std::jthread{std::move(t), cref(grid)}; } );

    std::vector<unsigned long> results;
    results.reserve(4);
    std::transform(futures.begin(), futures.end(), std::back_inserter(results), [](auto& f){ return f.get(); });

    assert(*std::max_element(results.cbegin(), results.cend()) == 70600674);

    std::cout << *std::max_element(results.cbegin(), results.cend()) << '\n';
}
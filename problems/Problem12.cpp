//
// Created by giuseppe on 22/11/2020.
//

#include <iostream>
#include <algorithm>
#include <cassert>
#include <ranges>
#include "../primes.hpp"
#include <fmt/ranges.h>

using Ull = unsigned long long;
using namespace std::ranges;

std::vector<Ull> factors(Ull n, std::shared_ptr<std::vector<Ull>> const& primes)
{
    std::vector<Ull> factors;
    factors.reserve(primes->size());
    factors.push_back(1);

    auto const prime_divisor {
        [N=n](auto const& p) {
            return N%p == 0;
        }
    };

    ranges::transform(views::filter(*primes, prime_divisor),
                      std::back_inserter(factors),
                      [N=n](auto const& p) mutable
                      {
                          while (N%p == 0) {
                              N /= p;
                          }
                          return p;
                      });

    return factors;
}


template<range R>
Ull number_of_divisors(Ull n, R const& r)
{
    return static_cast<Ull>(ranges::count_if(r, [n](auto const& d){ return n % d == 0; }));
}

int main()
{
    constexpr Ull times {500ULL};
    auto count {number_of_divisors(500ULL, views::iota(1ULL, 499ULL))};
    std::cout << count << '\n';

    constexpr Ull upperValue {1000};
    auto const primes {find_primes_up_to(upperValue*(upperValue+1)/4)};

    auto rangesOfTriangulars {
            views::iota(upperValue)
            | views::transform([](auto const& n){ return n*(n+1)/2; })
    };
    auto const found {
            ranges::find_if(rangesOfTriangulars,
                            [times, &upperValue](auto&& t){ return number_of_divisors(t, views::iota(1ULL, upperValue*(upperValue+1)/2)) > times; })
    };

    std::cout << *found << '\n';
    assert(*found == 76576500);
}
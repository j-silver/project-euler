#include <iostream>
#include <fstream>
#include <filesystem>
#include <charconv>
#include <cassert>
#include <array>

using Ull = unsigned long long;
constexpr auto maxDigits {std::numeric_limits<Ull>::digits10};

int main()
{
    std::ifstream fileStream {std::filesystem::path {"../problems/problem13.txt"}};
    unsigned long long sum {0};
    for (std::array<char, 13> charNumber;
         fileStream.read(charNumber.data(), 13);
         fileStream.ignore(50, '\n'))
    {
        unsigned long long value {0};
        std::from_chars(charNumber.data(), charNumber.data() + charNumber.size(), value);
        sum += value;
    }
    std::array<char, maxDigits> charSum;
    auto const result {std::to_chars(charSum.data(), charSum.data() + charSum.size(), sum)};
    assert(result.ec != std::errc::value_too_large);
    assert(std::string_view(charSum.data(), 10) == "5537376230");
    std::cout.write(charSum.data(), 10) << '\n';
}

#include <thread>
#include <algorithm>
#include <fmt/ranges.h>
#include <iostream>
#include <future>
#include <cassert>
#include <ranges>
#include <vector>

using Ull = unsigned long long;
using namespace std::ranges;
auto const first {[](auto const& val){ return val.first; }};

constexpr Ull upperLimit {1'000'000};
const unsigned maxThreads {std::jthread::hardware_concurrency()};

Ull next_collatz(Ull n)
{
    return n%2 == 0 ? n/2 : 3*n + 1;
}

std::pair<unsigned /* length */, Ull /* start */> chain_length(Ull n)
{
    unsigned length {1};
    for (Ull N {n}; N != 1; ++length) {
        N = next_collatz(N);
    }

    return {length, n};
}

std::pair<unsigned /* length */, Ull /* start */> max_chain_in_range(std::ranges::range auto const& rng)
{
    auto const chainLengths {views::transform(rng, [](auto const& n){ return chain_length(n); })};
    auto const maximum {std::ranges::max_element(chainLengths, std::less{}, first)};

    std::cerr << fmt::format("{}", *maximum) << '\n';
    return *maximum;
}

int main()
{
    auto const subRangeLength {upperLimit/static_cast<Ull>(maxThreads)};
    std::vector<iota_view<Ull, Ull>> subRanges;
    subRanges.reserve(maxThreads);
    for (unsigned t {0}; t < maxThreads; ++t) {
        iota_view const subRange {
            1ULL + (t*subRangeLength),
            std::min(upperLimit, 1 + subRangeLength + (t*subRangeLength))
        };
        subRanges.push_back(subRange);
    }

    std::vector<std::future<std::pair<unsigned, Ull>>> futures;
    futures.reserve(maxThreads);

    for (auto const& rng : subRanges) {
        futures.emplace_back(std::async(std::launch::async, max_chain_in_range<iota_view<Ull, Ull>>, rng));
    }

    std::vector<std::pair<int, int>> results;
    results.reserve(futures.size());
    transform(futures, std::back_inserter(results), [](auto& fut){ return fut.get(); });

    auto const result {
        max_element(results, std::less{}, [](auto const& res){ return res.first;  })
    };

    assert((*result).second == 837799);
    std::cout << (*result).second << '\n';
}
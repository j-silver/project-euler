//
// Created by giuseppe on 27/02/2021.
//

#include <stdexcept>
#include <iostream>
#include <cassert>
#include <cstdint>

double choose_half(int n)
{
    auto half {n/2.0};
    double val {1};
    while (half > 0) {
        val *= n/half;
        --n;
        --half;
    }
    return val;
}

int main(int argc, char* argv[])
{
    if (argc < 2) {
        throw std::invalid_argument{"Provide a number"};
    }
    auto const start {std::stoi(argv[1])};
    if (start <= 0 || start%2 != 0) {
        throw std::invalid_argument{"Must be even and positive"};
    }
    auto const answer {static_cast<uint64_t>(choose_half(start))};
    assert(answer == 137846528820);
    std::cout << start << " choose " << start/2 << " == " << answer << '\n';
}

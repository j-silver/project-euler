//
// Created by giuseppe on 27/02/2021.
//

#include <cmath>
#include "Problem16.h"

void double_number(std::vector<int>& decimal)
{
    int carry {0};
    for (auto& d : decimal) {
        if (d == 0) {
            d = carry;
            carry = 0;
            continue;
        }
        int const doubled {2*d};
        int const candidate {doubled + carry};
        carry = candidate > 9 ? 1 : 0;
        d = candidate%10;
    }
    if (carry > 0) {
        decimal.push_back(carry);
    }
}

std::vector<int> from_binary_to_decimal(unsigned int exponent)
{
    auto const decimalExponent {std::log10(2.0) * exponent};
    std::vector<int> dec (static_cast<std::vector<int>::size_type>(decimalExponent + 1), 0);
    dec[0] = 1;
    for (unsigned int p {1}; p <= exponent; ++p) {
        double_number(dec);
    }
    return dec;
}
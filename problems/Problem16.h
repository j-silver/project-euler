//
// Created by giuseppe on 27/02/2021.
//

#ifndef PROJECTEULER_PROBLEM16_H
#define PROJECTEULER_PROBLEM16_H

#include <vector>

void double_number(std::vector<int>& decimal);

std::vector<int> from_binary_to_decimal(unsigned int exponent);

#endif //PROJECTEULER_PROBLEM16_H

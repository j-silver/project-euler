//
// Created by giuseppe on 27/02/2021.
//

#include "Problem16.h"
#include <gtest/gtest.h>
#include <numeric>
#include <ranges>

TEST(Problem16, double_zero)
{
    std::vector<int> const start (1'000, 0);
    auto zero {start};
    double_number(zero);
    GTEST_CHECK_(zero == start);
}

TEST(Problem16, double_one)
{
    std::vector<int> const start (1'000, 1);
    std::vector<int> const end (1'000, 2);
    auto ones {start};
    double_number(ones);
    GTEST_CHECK_(ones == end);
}

TEST(Problem16, double_nineteen)
{
    std::vector<int> const start {9, 1};
    std::vector<int> const end {8, 3};
    auto nineteen {start};
    double_number(nineteen);
    GTEST_CHECK_(nineteen == end);
}

TEST(Problem16, double99)
{
    std::vector<int> const start {9, 9};
    std::vector<int> const end {8, 9, 1};
    auto result {start};
    double_number(result);
    GTEST_CHECK_(result == end);
}

TEST(Problem16, one)
{
    unsigned int const e {0};
    std::vector<int> const one {1};
    auto const result {from_binary_to_decimal(e)};
    GTEST_CHECK_(result == one);
}

TEST(Problem16, two)
{
    unsigned int const e {1};
    std::vector<int> const two {2};
    auto const result {from_binary_to_decimal(e)};
    GTEST_CHECK_(result == two);
}

TEST(Problem16, sixteen)
{
    unsigned int const e {4};
    std::vector<int> const sixteen {6, 1};
    auto const result {from_binary_to_decimal(e)};
    GTEST_CHECK_(result == sixteen);
}

TEST(Problem16, power_7)
{
    unsigned int const e {7};
    std::vector<int> const pow7 {8, 2, 1};
    auto const result {from_binary_to_decimal(e)};
    GTEST_CHECK_(result == pow7);
}

TEST(Problem16, power_15)
{
    unsigned int const e {15};
    auto const result {from_binary_to_decimal(e)};
    std::ranges::drop_while_view reverseResult {std::ranges::reverse_view{result}, [](auto const& d){ return d == 0; }};
    for (auto const& d : reverseResult) {
        std::cerr << d;
    }
    std::cerr << '\n';
    auto const sum {std::accumulate(result.cbegin(), result.cend(), 0)};
    std::cerr << "sum of digits: " << sum << '\n';
    GTEST_CHECK_(sum == 26);
}

TEST(Problem16, power_1000)
{
    unsigned int const e {1'000};
    auto const result {from_binary_to_decimal(e)};
    std::ranges::drop_while_view reverseResult {std::ranges::reverse_view{result}, [](auto const& d){ return d == 0; }};
    for (auto const& d : reverseResult) {
        std::cerr << d;
    }
    std::cerr << '\n';
    auto const sum {std::accumulate(result.cbegin(), result.cend(), 0)};
    std::cerr << "sum of digits: " << sum << '\n';
    GTEST_CHECK_(sum == 1366);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    assert(RUN_ALL_TESTS() == 0);
}
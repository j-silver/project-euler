//
// Created by giuseppe on 01/12/2020.
//

#include <string>
#include <map>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <ranges>
#include <numeric>
#include <algorithm>
#include <cassert>

const std::map<int, std::string> units {
        {0, ""},
        {1, "one"},
        {2, "two"},
        {3, "three"},
        {4, "four"},
        {5, "five"},
        {6, "six"},
        {7, "seven"},
        {8, "eight"},
        {9, "nine"},
        {10, "ten"},
        {11, "eleven"},
        {12, "twelve"},
        {13, "thirteen"},
        {14, "fourteen"},
        {15, "fifteen"},
        {16, "sixteen"},
        {17, "seventeen"},
        {18, "eighteen"},
        {19, "nineteen"}
};

const std::map<int, std::string> tens {
        {0, ""},
        {2, "twenty"},
        {3, "thirty"},
        {4, "forty"},
        {5, "fifty"},
        {6, "sixty"},
        {7, "seventy"},
        {8, "eighty"},
        {9, "ninety"}
};

std::string number_in_letters(int n)
{
    if (n < 1 || n > 1000) {
        throw std::invalid_argument{"number " + std::to_string(n) + " is out of limits"};
    }

    std::string words;

    if (n == 1000) {
        return "one thousand";
    }
    if (n <= 999 && n >= 100) {
        words = units.at(n/100) + " hundred";
        if (n%100 > 0) {
            words += " and ";
        }
    }
    if (int d {n%100}; d >= 20) {
        words += tens.at(d/10);
        if (d%10 > 0) {
            words += "-" + units.at(d%10);
        }
    }
    else {
        words += units.at(d);
    }

    return words;
}

auto count_letters(std::string const& word)
{
    return std::ranges::count_if(word, [](auto const& c){ return c != ' ' && c != '-'; });
}

int main()
{
    std::vector<int> const numbers {1000, 1, 999, 907, 882, 205, 220, 100, 400, 101, 99, 21, 20, 7, 11, 6};
    auto const words {std::ranges::views::transform(numbers, number_in_letters)};
    for (auto const& w : words) {
        std::cout << w << '\t' << count_letters(w) << '\n';
    }

    std::cout << "\n---\n";

    auto const counts {
            std::ranges::views::iota(1, 1001)
        |   std::ranges::views::transform(number_in_letters)
        |   std::ranges::views::transform(count_letters)
    };

    auto const answer {std::accumulate(counts.begin(), counts.end(), 0)};
    assert(answer == 21124);
    std::cout << answer << '\n';
}
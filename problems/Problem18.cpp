//
// Created by giuseppe on 01/03/2021.
//

#include <fstream>
#include <algorithm>
#include <ranges>
#include "Problem18.h"

namespace fs = std::filesystem;

std::vector<std::vector<int>> read_file(std::filesystem::path const& filePath)
{
    if (! fs::exists(filePath)) {
        throw std::invalid_argument{"File not found"};
    }

    auto const numberOfLines {
            [&filePath]() {
                std::ifstream fileStream {filePath};
                std::vector<int>::size_type count {0};
                for (std::string line; fileStream.good(); ) {
                    std::getline(fileStream, line);
                    if (! line.empty()) {
                        ++count;
                    }
                }
                return count;
            }()
    };

    std::vector<std::vector<int>> lines (numberOfLines);

    if (std::ifstream fileStream {filePath}; fileStream.good()) {
        std::vector<int>::size_type index {0};
        for (std::string line; fileStream.good(); ) {
            std::getline(fileStream, line);
            std::istringstream lineStream {line};
            lines[index].reserve(index+1);
            std::ranges::copy(std::ranges::istream_view<int>(lineStream), std::back_inserter(lines[index]));
            line.clear();
            ++index;
        }
    }
    else {
        throw std::invalid_argument{"Couldn't read the file"};
    }

    return lines;
}

int max_sum(std::vector<std::vector<int>> const& grid)
{
    std::vector<std::vector<int>> sums (grid.size());
    for (std::vector<int>::size_type l {grid.size()-1}; l != 0; --l) {
        if (l == grid.size()-1) {
            sums[l] = grid[l];
        }
        else {
            sums[l].resize(l+1);
            for (std::vector<int>::size_type i {0}; i <= l; ++i) {
                sums[l][i] = grid[l][i] + std::max(sums[l+1][i], sums[l+1][i+1]);
            }
        }
    }
    sums[0].push_back(grid[0][0] + std::max(sums[1][0], sums[1][1]));
    return sums[0][0];
}
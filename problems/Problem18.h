//
// Created by giuseppe on 01/03/2021.
//

#ifndef PROJECTEULER_PROBLEM18_H
#define PROJECTEULER_PROBLEM18_H

#include <vector>
#include <filesystem>

std::vector<std::vector<int>> read_file(std::filesystem::path const& filePath);

int max_sum(std::vector<std::vector<int>> const& grid);

#endif //PROJECTEULER_PROBLEM18_H

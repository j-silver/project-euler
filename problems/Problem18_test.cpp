//
// Created by giuseppe on 01/03/2021.
//

#include <gtest/gtest.h>
#include <fmt/ranges.h>
#include "Problem18.h"

TEST(Problem18, read_file)
{
    std::filesystem::path const testPath {"../problems/Problem18_test.txt"};
    auto const data {read_file(testPath)};
    std::cerr << fmt::format("{}", data);
}

TEST(Problem18, test_file)
{
    std::filesystem::path const testPath {"../problems/Problem18_test.txt"};
    auto const data {read_file(testPath)};
    auto const result {max_sum(data)};
    GTEST_CHECK_(result == 23);
}

TEST(Problem18, input)
{
    std::filesystem::path const path {"../problems/Problem_18.txt"};
    auto const data {read_file(path)};
    auto const result {max_sum(data)};
    GTEST_CHECK_(result == 1074);
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
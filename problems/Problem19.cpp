//
// Created by giuseppe on 07/03/2021.
//

#include <iostream>
#include <date/date.h>
using namespace date::literals;

int main()
{
    int count {0};
    for (date::year y {1901}; y <= date::year{2000}; ++y) {
        for (date::month m {date::January}; m <= date::December; ++m) {
            if (date::local_days const wd (date::year_month_day(y, m, date::day{1}));
                date::weekday(wd) == date::Sunday)
            {
                ++count;
            }
            if (m == date::December) {
                break;
            }
        }
    }

    std::cout << count << '\n';
}
#include <iostream>
#include <utility>
#include <vector>
#include <numeric>
#include <cassert>

using Ull = unsigned long long;

Ull next_fib(std::vector<Ull>& cache)
{
    if (cache.size() >= 2) {
        cache.emplace_back(cache.back() + *std::prev(cache.end(), 2));
        return cache.back();
    }
    throw std::invalid_argument("Cache must have at least 2 values");
}

class Fibonacci {
    std::vector<Ull> _cache;

public:
    explicit Fibonacci(Ull upperBound,
                       std::vector<Ull> cache = std::vector<Ull> {1, 2})
            : _cache {std::move(cache)}
    {
        while (_cache.back() < upperBound) {
            next_fib(_cache);
        }
        if (_cache.back() >= upperBound) {
            _cache.pop_back();
        }
    }

    [[nodiscard]] std::vector<Ull> const& get_numbers() const
    { return _cache; }
};

int main()
{
    constexpr Ull up {4'000'000};
    const Fibonacci F {up};

    const Ull sum {
            std::accumulate(F.get_numbers().cbegin(),
                            F.get_numbers().cend(),
                            Ull {0},
                            [](Ull const& s, Ull const& f) { return (f % 2 == 0 ? s + f : s); })
    };

    assert(sum == 4613732);

    std::cout << sum << '\n';
}

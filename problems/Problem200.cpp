#include "../primes.hpp"
#include "../extendedlong.hpp"
#include <algorithm>
#include <string>
#include <set>


bool is_prime_proof(long long l, std::shared_ptr<vector<Ull>> const& primes)
{
	ExtendedLong EL {l};
	std::vector<unsigned> n (EL.v());
	auto n_it {n.begin()};

	for (unsigned int digit : EL.v()) {
		for (unsigned i {0}; i <= 9; ++i) {
			if (i == digit || (i == 0 && n_it == n.end()-1))
				continue;
			else {
				*n_it = i;
				if (is_prime(static_cast<Ull>((long long) ExtendedLong{n}), primes))
					return false;
				else
					*n_it = digit;
			}
		}
		++n_it;
	}

	return true;
}


bool contains_substring_200(long long l)
{
	if (auto s {std::to_string(l)}; s.find("200") == std::string::npos)
		return false;

	return true;
}


class next_sqube {
	std::shared_ptr<std::vector<bool>> primes;
	std::set<long long> squbes;
	std::set<long long>::iterator latest;

	void generate_squbes()
	{
		for (size_t p {2}; p < primes->size(); ++p) {
			for (size_t q {p+1}; q < primes->size(); ++q) {
				if (primes->at(p) && primes->at(q))	{
					squbes.insert(p*p*q*q*q);
					squbes.insert(q*q*p*p*p);
				}
			}
		}

		latest = squbes.begin();
	}

public:
	explicit next_sqube(const Ull sup) :
		primes {which_are_primes(sup)}, squbes {}, latest {squbes.end()}
	{
		generate_squbes();
	}
	
	void reset(const Ull sup = 0)
	{
		squbes.clear();

		if (sup != 0) {
			primes = which_are_primes(sup);
			generate_squbes();
		}
		else
			primes.reset();
	}

	long long operator()()
	{
		long long value_to_return;

		if (latest != squbes.end()) {
			value_to_return = *latest;
			++latest;
		}
		else
			return -1;

		return value_to_return;
	}
};


int main()
{
	long long t1 {1992008};
	long long t2 {200};
	auto primes {find_primes_up_to(static_cast<Ull>(t1))};
	std::cout << std::boolalpha << is_prime_proof(t1, primes) << std::endl;
	std::cout << std::boolalpha << contains_substring_200(t1) << std::endl;
	std::cout << std::boolalpha << is_prime_proof(t2, primes) << std::endl;
	std::cout << std::boolalpha << contains_substring_200(t2) << std::endl;
	std::cout << std::boolalpha << contains_substring_200(993002) << std::endl;

	next_sqube NSQ {100};

	unsigned count {0};
	long long nsq {0};
	while (count < 200) {
		if (nsq = NSQ(); contains_substring_200(nsq) && is_prime_proof(nsq, primes)) {
			std::cout << nsq << ' ' << std::flush;
			++count;
		}
	}
	
	std::cout << '\n' << nsq << std::endl;
}

#include "../primes.hpp"
#include <iostream>
#include <algorithm>
#include <cassert>

constexpr Ull number {600'851'475'143};
constexpr Ull maxDivisor {100'000'000};



[[nodiscard]]
std::pair<Ull, Ull> factorise(Ull n, std::shared_ptr<std::vector<bool>> const& p)
{
    Ull maxFactor {p->size()};
    for (auto k {p->rbegin()}; k != p->rend(); ++k) {
        if (auto num {std::distance(k, std::prev(p->rend()))};
                *k && n%static_cast<Ull>(num) == 0)
        {
            maxFactor = static_cast<Ull>(num);
            break;
        }
    }

    auto const divided {n/maxFactor};
    return {maxFactor, divided};
}


int main()
{
    auto const primes {which_are_primes(maxDivisor)};
    auto const nextNumber {number};
    std::pair<Ull, Ull> factorisation;
    do {
        factorisation = factorise(nextNumber, primes);
    } while (! primes->at(factorisation.first));

    assert(factorisation.first == 6857);
    std::cout << factorisation.first;
}

#include <iostream>
#include <numeric>
#include "../extendedlong.hpp"
#include "../primes.hpp"
#include <fmt/ranges.h>
#include <cassert>
#include <ranges>
#include <algorithm>

using namespace std::ranges;

constexpr Ull sup {10'000'000};

const vector<ExtendedLong> one_digit_primes    {
    ExtendedLong{1}, ExtendedLong{2}, ExtendedLong{3}, ExtendedLong{5},
    ExtendedLong{7}
};


// Given a vector of same-length truncatables primes, return a vector of
// truncatables primes 1 digit longer than the original.
[[nodiscard]]
vector<ExtendedLong> one_more_digit(const vector<ExtendedLong>& truncatables,
                                    const shared_ptr<vector<Ull>>& primes)
{
    vector<ExtendedLong> one_more;

    for (const auto& t : truncatables) {
        auto v = t.v();
        for (unsigned i : views::iota(1U, 10U)) {
            v.push_back(i);
            ExtendedLong ev {v};
            if (is_prime(static_cast<Ull>(ev.operator long long()), primes)) {
                one_more.push_back(ev);
            }
            v.pop_back();
        }
    }

    return one_more;
}


// Trunc from right (that is, from the unit digit)
ExtendedLong trunc_from_right(const ExtendedLong& e)
{
    if (e.v().size() <= 1) {
        return ExtendedLong {};
    }
    vector<unsigned> vec (e.v().size()-1);
    std::copy(e.v().cbegin()+1, e.v().cend(), vec.begin());
    return ExtendedLong {vec, Sign::Plus};
}


int main()
{
    auto Primes {find_primes_up_to(sup)};
    vector<ExtendedLong> Trunc (one_digit_primes);
    vector<ExtendedLong> Truncatable;
    for (unsigned i {1}; i <= 7; ++i) {
        Trunc = one_more_digit(Trunc, Primes);
        for (const auto& T : Trunc) {
            auto t {T};
            while (!t.is_zero() &&
                   is_prime(static_cast<Ull>(t.operator long long()), Primes)) {
                t = trunc_from_right(t);
            }
            if (is_prime(static_cast<Ull>(t.operator long long()), Primes)) {
                Truncatable.push_back(T);
            }
        }
    }

    ExtendedLong sum {};
    for (const auto& t : Truncatable) {
        cout << t << endl;
        sum += t;
    }
    cout << "Sum: " << sum << '\n';


    auto one_at_its_ends = [](auto t)
    {
        return (t.v().front() != 1 && t.v().back() != 1);
    };

    cout << "Sum of the truncatable without ones at their ends ";
    auto withoutOnes {views::filter(Truncatable, one_at_its_ends)};
    std::for_each(withoutOnes.begin(), withoutOnes.end(), [](auto const& t){ std::cout << t << ' '; });
    std::cout << '\n';

    auto const answer {std::accumulate(withoutOnes.begin(), withoutOnes.end(), ExtendedLong{0})};
    assert(static_cast<long long>(answer) == 748317);
    cout << ")\n" << answer << '\n';
}

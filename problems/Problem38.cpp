#include <iostream>
#include <cmath>
#include <unordered_set>
using namespace std;

constexpr unsigned sup_limit {999999999};
constexpr unsigned inf_limit {99999999};
const unordered_set<unsigned> N { 1, 2, 3, 4, 5, 6, 7, 8, 9 };


unsigned candidate_pandigital(unsigned x, unsigned n)
{
	unsigned pan {0};
	unsigned latest_power {1};
	for (unsigned i {1}; i <= n; ++i) {
		auto product = x*i;
		auto product_copy {product};
		while ((product_copy /= 10) > 0)
			++latest_power;
		pan = pan*static_cast<unsigned>(pow(10, latest_power)) + product;
		latest_power = 1;
	}

	return pan;
}


bool is_nine_digit(unsigned p) { return (p > inf_limit && p <= sup_limit); }


bool is_pandigital(unsigned p)
{
	unordered_set<unsigned> digits;
	while (p > 0) {
		digits.emplace(p%10);
		p /= 10;
	}
	return (digits == N);
}


int main()
{
	unsigned temp_max {0};
	for (unsigned n {2}; n <= 9; ++n) {
		for (unsigned start {1}; pow(start, n) <= sup_limit; ++start) {
			auto pan = candidate_pandigital(start, n);
			if (pan > sup_limit)
				break;
			if (is_nine_digit(pan) && is_pandigital(pan))
				temp_max = max(pan, temp_max);
		}
	}
	cout << temp_max << endl;
}


//
// Created by giuseppe on 13/02/2021.
//

#include <set>
#include <stdexcept>
#include <numeric>
#include <iostream>
#include <experimental/source_location>
#include <sstream>
#include <cmath>
#include <fmt/ranges.h>
#include <map>

std::ostringstream& operator<<(std::ostringstream& msg, std::experimental::source_location const& location)
{
    msg << "[" << location.function_name() << ": " << location.line() << "] ";
    return msg;
}

std::experimental::source_location const& src_loc(std::experimental::source_location const& loc = std::experimental::source_location::current())
{
    return loc;
}

int perimeter(std::set<int> const& polygon)
{
    return std::accumulate(polygon.cbegin(), polygon.cend(), 0);
}

bool is_right(std::set<int> const& triangle)
{
    if (triangle.size() != 3) {
        std::ostringstream msg;
        msg << src_loc() << "Not a triangle";
        throw std::invalid_argument{msg.str()};
    }

    int const a {*triangle.cbegin()};
    int const b {*std::next(triangle.cbegin())};
    int const c {*std::next(triangle.cbegin(), 2)};

    return a*a + b*b == c*c;
}

int count_right_angle(int p)
{
    int count {0};
    for (int b {1}; b <= p-2; ++b) {
        for (int a {1}; a <= b && a*a + b*b <= p*p; ++a) {
            double const c {std::sqrt(a*a + b*b)};
            if (std::fabs(c - std::floor(c)) <= std::numeric_limits<double>::epsilon()*std::fabs(c + std::floor(c))) {
                std::set<int> t {a, b, static_cast<int>(c)};
                if (is_right(t) && perimeter(t) == p) {
                    ++count;
                }
            }
        }
    }
    return count;
}

std::map<int, int> max_values(int p)
{
    std::map<int, int> maxValues {{1, 25}};
    for (int perim {p}; perim >= 3; --perim) {
        if (auto const count {count_right_angle(perim)}; count > std::prev(maxValues.cend())->first) {
            maxValues.emplace(count, perim);
        }
    }
    return maxValues;
}

int main(int argc, char* argv[])
{
    if (argc != 4) {
        std::ostringstream msg;
        msg << src_loc() << "Please provide 3 sides";
        throw std::invalid_argument{msg.str()};
    }
    std::set<int> const triangle {std::stoi(argv[1]), std::stoi(argv[2]), std::stoi(argv[3])};

    std::cout << std::boolalpha << is_right(triangle) << '\n';
    std::cout << "perimeter: " << perimeter(triangle) << '\n';

    std::cout << "Number of right triangles with perimeter == 120: " << count_right_angle(120) << '\n';

    auto const maxValues {max_values(1'000)};
    std::cout << std::prev(maxValues.cend())->second << '\n';
}
//
// Created by giuseppe on 20/11/2020.
//

#include <charconv>
#include <array>
#include <system_error>
#include <ranges>
#include <iostream>
#include <algorithm>
#include <cassert>

using namespace std::ranges;
using sz_type = std::string_view::size_type;
constexpr size_t maxDigits {7};

bool is_palyndrome(int n)
{
    std::array<char, maxDigits> charRepresentation{};
    auto const result {std::to_chars(charRepresentation.data(), &charRepresentation[maxDigits-1], n)};
    if (result.ec == std::errc::value_too_large)
    {
        auto const errorCondition {std::make_error_condition(result.ec)};
        throw std::overflow_error(errorCondition.message());
    }
    auto *const start {charRepresentation.data()};
    auto const semiwidth {static_cast<sz_type>(std::distance(start, result.ptr)/2)};
    std::string_view const representation {start, result.ptr};
    return equal(take_view(reverse_view(representation), semiwidth), take_view(representation, semiwidth));
}

int main()
{
    std::boolalpha(std::cout);
    std::cout << is_palyndrome(101) << '\n';
    std::cout << is_palyndrome(123) << '\n';
    std::cout << is_palyndrome(000'000) << '\n';

    int maximum {1};
    for (int const k : iota_view(100, 999)) {
        for (int const j : iota_view(k+1, 999)) {
            auto const product {j*k};
            if (is_palyndrome(product)) {
                maximum = std::max(maximum, product);
            }
        }
    }

    assert(maximum == 906609);
    std::cout << maximum << '\n';
}
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include "../extendedlong.hpp"


constexpr int max_pow_of_ten {6};


// find the digits d(10^n) in the range of number with 'power' n. of digits
unsigned same_digits(const int power, std::ofstream& file,
					const std::vector<unsigned>& PS)
{
	unsigned product {1};
	unsigned counter {0};
	auto maximum {static_cast<unsigned>(std::pow(10, power))};
	const ExtendedLong e_maximum {maximum};
	for (ExtendedLong i {1}; i <= e_maximum && counter <= maximum; ++i) {
		for (auto digit {i.v().crbegin()}; digit != i.v().crend(); ++digit) {
			++counter;
			if (std::find(PS.cbegin(), PS.cend(), counter) != PS.cend()) {
				product *= *digit;
				std::cout << "*" << *digit;
				file << i << "\n";
			}
		}
	}
	return product;
}


int main()
{
	std::vector<unsigned> powers;
	for (int p {0}; p <= max_pow_of_ten; ++p) 
		powers.push_back(static_cast<unsigned>(std::pow(10, p)));

	unsigned result {1};
	std::cout << result << " ";

	std::ofstream f {"p40.txt"};
	result = same_digits(max_pow_of_ten, f, powers);

	std::cout << " = " << result << std::endl;
}



#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>

bool check_word(const std::string& w)
{
	unsigned long long value {0};
	for (const auto& c : w)
		value += (c - 'A' + 1);

	unsigned long long n {1};
	while (n*(n+1)/2 < value)
		++n;

	if (n*(n+1)/2 == value)
		return true;

	return false;
}



int main()
{
	std::ifstream file {"p042_words.txt"};
	unsigned count {0};

	for (std::string W; getline(file, W, ',').rdstate() == std::ios_base::goodbit; ) {
		W.pop_back();
		if (check_word(W.substr(1)))
			++count;
	}

	std::cout << count << std::endl;
}

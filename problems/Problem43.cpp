#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <iostream>

std::vector<unsigned> primes {2, 3, 5, 7, 11, 13, 17};

std::vector<unsigned> digits {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};


bool check_number(std::vector<unsigned>& v)
{
	for (auto&& it {v.begin()+1}; it != v.end()-2; ++it) {
		unsigned number {100* *it + 10* *(it+1) + *(it+2)};
		if (number%primes[std::distance(digits.begin(), it)-1] != 0)
			return false;
		else
			continue;
	}
	return true;
}


int main()
{
	unsigned long long sum {0};

	while (std::next_permutation(digits.begin(), digits.end())) {
		if (check_number(digits)) {
			for (const auto& d : digits)
				std::cout << d;
			std::cout << std::endl;
			unsigned long long factor {1};
			sum = std::accumulate(digits.crbegin(), digits.crend(), sum,
								  [&factor=factor](unsigned long long s, unsigned d)
								  {
								  	s += d*factor;
								  	factor *= 10;
								  	return s;
								  }
			);
		}
	}

	std::cout << "---\n" << sum << std::endl;
}

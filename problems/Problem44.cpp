// Problem44

#include <stdexcept>
#include <complex>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>


using ULL = unsigned long long;


constexpr ULL pentagonal(ULL k)
{
	return k*(3*k-1)/2;
}


[[nodiscard]]
std::unique_ptr<std::vector<ULL>> generate_pentagonals(ULL limit = 100'000)
{
	auto pentagonals {std::make_unique<std::vector<ULL>>()};
	pentagonals->reserve(limit);
	for (ULL n {1}; n <= limit; ++n)
		pentagonals->push_back(pentagonal(n));

	return pentagonals;
}


bool is_pentagonal(ULL n)
{
	auto s {sqrt(1.0 + 24.0*static_cast<double>(n))};

	if (s != std::floor(s))
		return false;

	return (1 + static_cast<ULL>(s))%6 == 0;
}


bool is_pentagonal_fast(ULL n, std::unique_ptr<std::vector<ULL>>& pents)
{
	return std::binary_search(pents->begin(), pents->end(), n);
}


// pentagonal(k+j) - pentagonal(k)
constexpr ULL pentagonal_difference(ULL k, ULL j)
{
	return (3*j*j + (6*k - 1)*j)/2;
}


void increment(ULL& k, ULL& j)
{
	if (k == 1) {
		k = j+1;
		j = 1;
	}
	else {
		--k;
		++j;
	}
}


ULL find_pentagonal_difference()
{
	ULL j {1};
	ULL k {1};

	ULL pentagonalDifference;

	while (true) {
		pentagonalDifference = pentagonal_difference(k, j);
		while (!is_pentagonal(pentagonalDifference)) {
			increment(k, j);
			pentagonalDifference = pentagonal_difference(k, j);
		}

		if (is_pentagonal(pentagonalDifference + 2*pentagonal(k)))
			break;

		increment(k, j);
	}

	return pentagonalDifference;
}


int main()
{
	std::cout << find_pentagonal_difference() << '\n';

	return 0;
}

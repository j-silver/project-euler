#include <iostream>
#include <limits>

using Ull = unsigned long long;
constexpr auto max_unsigned = std::numeric_limits<unsigned>::max();


constexpr Ull triangular(unsigned n) { return n * (n + 1) / 2; }

constexpr Ull pentagonal(unsigned n) { return n * (3 * n - 1) / 2; }

constexpr Ull hexagonal(unsigned n) { return n * (2 * n - 1); }


constexpr auto all_of_3(unsigned Tstart, unsigned Pstart, unsigned Hstart)
{
	while (triangular(Tstart) != pentagonal(Pstart)
			&& pentagonal(Pstart) != hexagonal(Hstart)) {
		if (triangular(Tstart) < pentagonal(Pstart)) {
			++Tstart;
			continue;
		}
		else if (triangular(Tstart) > pentagonal(Pstart)) {
			++Pstart;
			continue;
		}
		if (pentagonal(Pstart) < hexagonal(Hstart)) {
			++Pstart;
			continue;
		}
		else if (pentagonal(Pstart) > hexagonal(Hstart)) {
			++Hstart;
			continue;
		}
	}

	return std::pair<Ull, unsigned>{triangular(Tstart), Tstart};
}


int main()
{
	for (unsigned start {2}; start < max_unsigned; ++start) {
		auto [result, new_start] = all_of_3(start, start, start);
		std::cout << new_start << ": " << result << std::endl;
		start = new_start;
	}
}



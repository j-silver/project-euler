//
// Created by giuseppe on 07/06/19.
//

#include <memory>
#include <vector>
#include <cmath>
#include <iostream>
#include "../primes.hpp"


bool is_prime_plus_double_square(std::shared_ptr<std::vector<bool>> const& p, int n)
{
	size_t current {1};
    size_t N {static_cast<size_t>(n)};

	while (current < N) {
		if (p->at(current) && current%2 == 1) {
			auto squareRoot {floor(sqrt((N - current)/2))};
			if (squareRoot*squareRoot == static_cast<double>((N - current)/2))
				break;
		}
		++current;
	}

	return current < N;
}


int main()
{
	auto primes {which_are_primes(1'000'000)};

	int composite {0};
	bool found {false};

	for (int j {1}; !found; ++j) {
		for (int k {1}; k <= j; ++k) {
			composite = (2*j+1)*(2*k+1);
			if (is_prime_plus_double_square(primes, composite))
				continue;
			else {
				found = true;
				break;
			}
		}
	}

	std::cout << composite << '\n';
	return 0;
}

#include <utility>
#include <set>
#include <algorithm>
#include <iostream>
#include "../primes.hpp"

constexpr Ull sup {1'000'000};
constexpr int num {4};


class Factorisation {
	std::shared_ptr<std::vector<Ull>> primes;
public:
	explicit Factorisation(std::shared_ptr<std::vector<Ull>> primes_) :
		primes {std::move(primes_)} {}

	size_t unique_factors(Ull n)
	{
		std::set<Ull> factors;

		for (auto p : *primes) {
			if (p > n) {
				break;
			}
			if (n%p == 0) {
				factors.emplace(p);
				do {
					n /= p;
				}
				while (n%p == 0);
			}
		}

		return factors.size();
	}
};


int main()
{
	auto primeNumbers {find_primes_up_to(sup)};
	Factorisation F {primeNumbers};

	for (Ull n {2 * 3 * 5}; n < sup; ++n) {
		int count {0};
		for (auto i {n}; i < n + num; ++i) {
			auto factors {F.unique_factors(i)};
			if (factors != num)
				break;
			else
				++count;
		}
		if (count == num) {
			std::cout << '\n' << n << '\n';
			break;
		}
	}
}

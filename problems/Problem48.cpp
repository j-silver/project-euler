#include <iostream>

using Ull = unsigned long long;

constexpr Ull modulo {10'000'000'000};

Ull n_to_the_power_of_n(Ull n)
{
	Ull result {1};
	for (Ull j {0}; j < n; ++j) {
	    result = result*n%modulo;
	}

	return result;
}


int main()
{
	Ull sum {0};
	for (Ull i {1}; i <= 1000; ++i) {
	    sum += n_to_the_power_of_n(i)%modulo;
	}

	std::cout << sum%modulo<< '\n';
}
#include <iostream>
#include <algorithm>
#include "../primes.hpp"

using Ull = unsigned long long;


int main()
{
	auto rangeOfPrimes {find_primes_up_to(9999)};
	auto fourDigitPrimes {find_primes_in_range(1000, 9999, rangeOfPrimes)};

	std::vector<Ull> found;

	for (auto p : fourDigitPrimes) {
		if (std::find(found.begin(), found.end(), p) != found.end())
			continue;
		auto sequence {std::to_string(p)};
		while (std::next_permutation(sequence.begin(), sequence.end())) {
			auto next {std::stoull(sequence)};
			if (is_prime(next, rangeOfPrimes)) {
				auto third {next + next - p};
				if (is_prime(third, rangeOfPrimes)) {
					auto thirdSequence {std::to_string(third)};
					if (std::is_permutation(thirdSequence.begin(),
							                thirdSequence.end(),
							                sequence.begin())) {
						std::cout << p << next << third << '\n';
						found.push_back(p);
					}
				}
			}
		}
	}
	return 0;
}
//
// Created by giuseppe on 20/11/2020.
//

#include <numeric>
#include <iostream>
#include <ranges>
#include <algorithm>
#include <fmt/ranges.h>
#include <cassert>
#include <vector>

uint64_t mcm(std::integral auto t)
{
    return t;
}

uint64_t mcm(std::integral auto t1, std::integral auto t2)
{
    return std::lcm(t1, t2);
}

template<typename ...T>
uint64_t mcm(std::integral auto head, T... tail)
{
    return mcm(head, mcm(tail...));
}

template<std::forward_iterator F>
uint64_t mcm(F const start, std::sentinel_for<F> auto const end)
{
    if (std::next(start) == end) {
        return *start;
    }
    if (std::distance(start, end) == 2) {
        return std::lcm(*start, *std::next(start));
    }
    return std::lcm(*start, mcm(std::next(start), end));
}

int main()
{
    std::cout << mcm(3, 4, 5) << '\n';
    std::cout << mcm(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) << '\n';
    std::vector<uint64_t> firstTwenty (20, 0);
    std::iota(firstTwenty.begin(), firstTwenty.end(), 1);
    std::cout << fmt::format("{}\n", firstTwenty);

    auto const answer {mcm(firstTwenty.cbegin(), firstTwenty.cend())};
    assert(answer == 232792560);
    std::cout << answer << '\n';
}
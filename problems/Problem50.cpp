#include <iostream>
#include "../primes.hpp"

using Ull = unsigned long long;

constexpr Ull sup {1'000'000};



int main()
{
	auto primes {find_primes_up_to(sup)};

	int maxSequence {1};
	Ull result {0};

	for (auto start {primes->begin()}; start != primes->end(); ++start) {
		Ull sum {0};
		int tempSequence {0};
		Ull lastPrime {2};
		for (auto p {start}; p != primes->end(); ++p) {
			if (sum >= sup)
				break;
			sum += *p;
			lastPrime = *p;
			++tempSequence;
		}

		sum -= lastPrime;
		if (is_prime(sum, primes) && tempSequence > maxSequence) {
			result = sum;
			maxSequence = tempSequence;
			std::cout << sum << ' ' << maxSequence << '\n';
		}
	}

	std::cout << result << '\n';
	return 0;
}
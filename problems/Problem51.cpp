#include <iostream>
#include <algorithm>
#include <thread>
#include <future>
#include <cmath>
#include <numeric>
#include "../primes.hpp"
#include "../subsets.h"


using StringIt = std::string::const_iterator;
using Substrings = Subsets<StringIt>;

template <typename func>
using pkg_tsk = std::packaged_task<func>;

using sPtr = std::shared_ptr<std::vector<Ull>>;

using scanFunc =
	std::vector<std::pair<std::vector<int>, long>>(
			std::vector<Ull>::const_iterator const&,
			std::vector<Ull>::const_iterator const&,
		   	size_t,
		   	sPtr const&);


std::vector<int>
sequence_of_numbers(std::string const& startingNumber,
					std::vector<StringIt> const& digitsToBeReplaced)
{
	auto const start {startingNumber.begin()};
	if (std::distance(start, digitsToBeReplaced.back())
			== static_cast<long>(startingNumber.size()-1))
		return std::vector<int>{};

	std::vector<int> sequence;
	sequence.reserve(10);

	auto copyOfStartingNumber {startingNumber};

	for (int n {0}; n <= 9; ++n) {
		for (auto const& digit : digitsToBeReplaced)
			copyOfStartingNumber.
				replace(static_cast<std::string::size_type>(std::distance(start, digit)), 1, std::to_string(n));
		if (copyOfStartingNumber.front() != '0')
			sequence.push_back(std::stoi(copyOfStartingNumber));
	}

	return sequence;
}


auto how_many_are_primes(std::shared_ptr<std::vector<Ull>> const& primes,
						 std::vector<int> const& nums)
{
	std::vector<int> theseArePrimes;

	std::copy_if(nums.cbegin(), nums.cend(), theseArePrimes.begin(),
	             [&primes](int const number){ return is_prime(static_cast<Ull>(number), primes); });

	return theseArePrimes;
}


std::vector<std::pair<std::vector<int>, long>>
scan_range_single_thread(std::vector<Ull>::const_iterator const& start,
						 std::vector<Ull>::const_iterator const& end,
						 size_t digits,
						 std::shared_ptr<std::vector<Ull>> const& p)
{
	std::vector<std::pair<std::vector<int>, long>> result;
	long maximum {0};

	for (auto number {start}; number != end; ++number) {
		auto const numberString {std::to_string(*number)};
		for (size_t replace {1}; replace < digits; ++replace) {
			Subsets const subs {numberString.begin(), numberString.end()-1};
			auto const allSubsets {subs.build(replace)};
			for (auto const& subset : allSubsets) {
				auto const numbers {sequence_of_numbers(numberString, subset)};
				auto const howMany {how_many_are_primes(p, numbers)};
				if (static_cast<long>(howMany.size()) > maximum)
				{
					result.emplace_back(howMany, howMany.size());
					maximum = static_cast<long>(howMany.size());
				}
			}
		}
	}

	return result;
}


void scan_ranges(size_t digits, std::vector<Ull> const& ps, sPtr const& p)
{
	std::cout << "_start: " << ps.front() << ", end: " << ps.back() << '\n';
	std::cout << "-----------------------\n";

	std::vector<std::pair<std::vector<int>, long>> result;

	auto nOfThreads {std::thread::hardware_concurrency() + 1};
	auto intervalRange {ps.size()/nOfThreads};

	std::vector<
		std::pair<std::vector<Ull>::const_iterator,
				  std::vector<Ull>::const_iterator>
	> intervals;

	for (size_t i {0}; i < nOfThreads; ++i)
		intervals.emplace_back(ps.begin() + i*intervalRange,
							   ps.begin() + (i + 1)*intervalRange);

	std::vector<pkg_tsk<scanFunc>> tasks;
	for (size_t taskId {0}; taskId < nOfThreads; ++taskId)
		tasks.emplace_back(scan_range_single_thread);

	std::vector<std::future<
	    std::vector<std::pair<std::vector<int>, long>>>
	> futures;

	for (auto& task : tasks)
		futures.emplace_back(task.get_future());

	std::vector<std::thread> threads;
	for (size_t taskId {0}; taskId < nOfThreads; ++taskId)
		threads.emplace_back(std::move(tasks[taskId]),
				             intervals[taskId].first, 
				             intervals[taskId].second,
				             digits,
				             p);

	for (auto& t : threads)
		if (t.joinable())
			t.detach();

	for (auto& f : futures) {
		for (auto&& v : f.get()) {
			for (auto const& n : v.first)
				std::cout << n << ' ';
			std::cout << '\n';
		}
	}
}


int main(int argc, char* argv[])
{
	if (argc < 2)
		throw std::invalid_argument{"Please provide an upper limit"};

	auto upLimit {std::stoull(argv[1])};
	auto up {upLimit};

	auto primes {find_primes_up_to(up)};
	Ull powerOfTen {0};
	while ((up /= 10) > 0) {
		++powerOfTen;
	}

	auto primesInRange {
		find_primes_in_range(static_cast<Ull>(std::pow(10, powerOfTen)), upLimit, primes)
	};

	scan_ranges(powerOfTen, primesInRange, primes);
}

#include "../subsets.cpp"
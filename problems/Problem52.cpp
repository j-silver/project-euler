#include <string>
#include <algorithm>
#include <iostream>
#include <cmath>

using namespace std;

bool permuted_multiples(unsigned long candidate)
{
	auto letters {to_string(candidate)};
	auto sixTimes {6*candidate};

	while (sixTimes != candidate)
	{
		auto sixTimesInLetters = to_string(sixTimes);
		if (! is_permutation(sixTimesInLetters.begin(),
                             sixTimesInLetters.end(),
							 letters.begin()))
			break;
		sixTimes -= candidate;
	}

	return sixTimes == candidate;
}


int main()
{
	unsigned long i {1};
	while (! permuted_multiples(i))  {
		if (floor(std::log10(6*i)) > log10(i))
			i = static_cast<unsigned long>(pow(10, floor(log10(6*i))));
		++i;
	}

	cout << i << '\n';
}

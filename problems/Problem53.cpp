//
// Created by giuseppe on 05/09/2019.
//


#include <iostream>

bool is_greater_than_million(int n, int k)
{
    double result {1};
    while (k > 0 && result < 1'000'000)
        result *= static_cast<double>(n--)/static_cast<double>(k--);

    return result > 1'000'000;
}

int main()
{
    int count {0};

    for (int n {1}; n <= 100; ++n) {
        int k {0};
        while (k <= n && !is_greater_than_million(n, k))
            ++k;
        if (k <= n)
            count += n + 1 - 2*k;
    }

    std::cout << count << '\n';
}
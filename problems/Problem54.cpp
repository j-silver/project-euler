//
// Created by giuseppe on 12/08/2019.
//

#include <stdexcept>
#include <cassert>
#include <array>
#include <tuple>
#include <set>

enum class HandValue : int {
    high_card = 0, one_pair, two_pairs, three_of_a_kind, straight,
    flush, full_house, four_of_a_kind, straight_flush, royal_flush
};

enum class Suit : char {
    spear = 'S', club = 'C', diamond = 'D', heart = 'H'
};

class Card {
public:
    explicit Card(std::pair<int, Suit> c) : _c {std::move(c)}
    {
        check_valid();
    }

    explicit Card(std::string const& s) : _c {}
    {
        if (s.length() == 2) {
            _c = {static_cast<int>(s[0] - '0'), static_cast<Suit>(s[1])};
        }
        check_valid();
    }

    explicit Card(std::pair<char, Suit> c) : _c {-1, c.second}
    {
        if (c.first == 'J') {
            _c.first = 11;
        }
        else if (c.first == 'Q') {
            _c.first = 12;
        }
        else if (c.first == 'K') {
            _c.first = 13;
        }
        else if (c.first == 'A') {
            _c.first = 14;
        }
        else {
            throw std::invalid_argument{"Invalid card"};
        }
    }

    constexpr friend std::weak_ordering operator<=>(Card const& lhs, Card const& rhs)
    {
        return std::weak_order(lhs._c.first, rhs._c.first);
    }

private:
    std::pair<int, Suit> _c;

    void check_valid() const
    {
        if (_c.first < 2 || _c.first > 10) {
            throw std::invalid_argument{"Invalid card"};
        }
    }
};

class Hand {
public:
    explicit Hand(std::array<Card, 5> h) : _hand {std::move(h)} {}

    explicit Hand(std::tuple<Card, Card, Card, Card, Card> h)
    : _hand { std::get<0>(h), std::get<1>(h), std::get<2>(h), std::get<3>(h), std::get<4>(h)}
    {
    }

    int value() const;

private:
    std::array<Card, 5> _hand;
};


int main()
{
    assert(HandValue::one_pair < HandValue::straight_flush);
    assert((Card{std::make_pair('K', Suit::diamond)} < Card{std::make_pair('A', Suit::diamond)}));
    assert(static_cast<Suit>('C') == Suit::club);
    assert(static_cast<Suit>('C') != Suit::diamond);
    assert(Card{"2C"} < Card{"3S"});
    assert(! (Card{"2C"} < Card{"2H"}) && ! (Card{"2C"} > Card{"2H"}));
    assert(Card{"3S"} <= Card{"3D"} && Card{"3S"} >= Card{"3D"});
}


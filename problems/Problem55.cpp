//
// Created by giuseppe on 07/09/2019.
//

#include <algorithm>
#include <numeric>
#include <set>
#include <iostream>
#include <ranges>
#include <boost/multiprecision/gmp.hpp>

using very_long = boost::multiprecision::mpz_int;


bool is_lychrel(very_long const& candidate, std::set<very_long>& lychrels)
{
    auto copyOfCandidate {candidate};
    for (int&& [[maybe_unused]] attempt : std::ranges::views::iota(0, 50)) {
        if (std::ranges::binary_search(lychrels, copyOfCandidate)) {
            return true;
        }

        auto stringCandidate {copyOfCandidate.str()};
        std::ranges::reverse(stringCandidate);
        auto candidateView {std::ranges::views::drop_while(stringCandidate, [](char c){ return c == '0'; })};
        try {
            std::string candidateFromView;
            candidateFromView.reserve(stringCandidate.size());
            std::ranges::copy(candidateView, std::back_inserter(candidateFromView));
            very_long const sum {very_long {candidateFromView} + copyOfCandidate};
            auto const stringSum {sum.str()};
            auto copyOfStringSum {stringSum};
            std::ranges::reverse(copyOfStringSum);
            auto copyView {std::ranges::views::drop_while(copyOfStringSum,
                                       [](char c){ return c == '0'; })};
            std::string copyViewString;
            copyViewString.reserve(copyOfStringSum.size());
            std::ranges::copy(copyView, std::back_inserter(copyViewString));
            try {
                if (very_long {copyViewString} == sum) {
                    return false;
                }
                copyOfCandidate = sum;
            }
            catch(std::exception const& e) {
                std::cerr << "\ncopyOfStringSum=" << copyOfStringSum
                    << " sum=" << sum << " stringCandidate="
                    << stringCandidate << " copyOfCandidate="
                    << copyOfCandidate << '\n';
                std::terminate();
            }
        }
        catch(std::exception const& e) {
            std::cerr << "\nstringCandidate=" << stringCandidate
                << " copyOfCandidate=" << copyOfCandidate << " candidate="
                << candidate << '\n';
            std::terminate();
        }
    }

    lychrels.emplace(candidate);
    return true;
}


int main()
{
    std::set<very_long> lychrels;

    for (very_long candidate {1}; candidate < 10'000; ++candidate) {
        if (is_lychrel(candidate, lychrels))
            std::cerr << candidate << ' ';
    }

    std::cout << '\n' << lychrels.size();
}

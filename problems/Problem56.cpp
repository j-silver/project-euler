//
// Created by giuseppe on 07/03/2021.
//

#include <numeric>
#include "Problem56.h"

int powerful_digit_sum(int base, int exponent)
{
    cpp_int result {1};
    for (int e {1}; e <= exponent; ++e) {
        result *= base;
    }
    std::string digits;
    digits.reserve(10'000);
    std::ostringstream digitsStream {digits};
    digitsStream << result;
    auto letters {digitsStream.str()};
    auto sum {
        std::accumulate(letters.cbegin(), letters.cend(),
                        0,
                        [](auto init, auto const& c){ return init += c - '0'; })};
    return sum;
}

int search_max_sum(int maxBase, int maxExponent)
{
    int currentMax {0};
    for (int b {1}; b <= maxBase; ++b) {
        for (int e {1}; e <= maxExponent; ++e) {
            currentMax = std::max(currentMax, powerful_digit_sum(b, e));
        }
    }

    return currentMax;
}

int main()
{
    std::cout << search_max_sum(99, 99) << '\n';
}

//
// Created by giuseppe on 07/03/2021.
//

#ifndef PROJECTEULER_PROBLEM56_H
#define PROJECTEULER_PROBLEM56_H

#include <boost/multiprecision/cpp_int.hpp>

using boost::multiprecision::cpp_int;

int powerful_digit_sum(int base, int exponent);

int search_max_sum(int maxBase, int maxExponent);

#endif //PROJECTEULER_PROBLEM56_H

//
// Created by giuseppe on 05/11/22.
//

#include <ranges>
#include "Problem57.h"

mpq_class next(mpq_class const& prev)
{
    return {2*prev.get_den() + prev.get_num(), prev.get_den() + prev.get_num()};
}

bool numerator_has_more_digits(mpq_class const& q)
{
    auto const slash {q.get_str().find('/')};
    auto const num {q.get_str().substr(0, slash)};
    auto const den {q.get_str().substr(slash+1)};
    return num.size() > den.size();
}

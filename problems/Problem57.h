//
// Created by giuseppe on 05/11/22.
//

#ifndef PROJECTEULER_PROBLEM57_H
#define PROJECTEULER_PROBLEM57_H

#include <gmpxx.h>

mpq_class next(mpq_class const& prev);
bool numerator_has_more_digits(mpq_class const& q);

#endif //PROJECTEULER_PROBLEM57_H

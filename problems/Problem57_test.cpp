//
// Created by giuseppe on 05/11/22.
//

#include "Problem57.h"
#include <gtest/gtest.h>
#include <exception>
#include <random>


TEST(Problem57, Next)
{
    mpq_class const f {"3/2"};
    auto const n {next(f)};
    GTEST_CHECK_(n.get_num() == 7 && n.get_den() == 5);

    std::random_device randomDevice {"/dev/urandom"};
    std::uniform_int_distribution distribution {1, 1'000'000};
    auto const a {distribution(randomDevice)};
    auto const b {distribution(randomDevice)};
    auto const n2 {next(mpq_class {a, b})};
    GTEST_CHECK_(n2.get_num() == 2*b + a && n2.get_den() == b + a);
}

TEST(Problem57, first_7)
{
    std::vector<mpq_class> first_7;
    mpq_class const start {3, 2};
    first_7.push_back(start);
    for (int i {0}; i < 7; ++i)
    {
        auto const append {next(first_7.back())};
        first_7.push_back(append);
    }

    std::vector<mpq_class> const results {
         {3, 2}, {7, 5}, {17, 12}, {41, 29}, {99, 70}, {239, 169}, {577, 408}, {1393, 985}
    };

    ASSERT_EQ(first_7, results);
}

TEST(Problem57, num_has_more_digits)
{
    mpq_class const f {1390, 245};
    ASSERT_TRUE(numerator_has_more_digits(f));

    mpq_class f2 {1002, 501};
    f2.canonicalize();
    ASSERT_FALSE(numerator_has_more_digits(f2));
}

TEST(Problem57, Solution)
{
    unsigned count {0};
    mpq_class f {3, 2};
    for (int i {1}; i < 1'000; ++i)
    {
        f = next(f);
        f.canonicalize();
        if (numerator_has_more_digits(f))
        {
            ++count;
        }
    }

    std::cout << count << '\n';
    GTEST_ASSERT_EQ(count, 153);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
//
// Created by giuseppe on 06/10/2019.
//

#include "Problem58.h"

#include <cmath>
#include <algorithm>

[[nodiscard]]
std::list<Ull> next_layer(std::list<Ull> const& layerVertices)
{
    auto const last {layerVertices.back()};
    Ull const side {static_cast<Ull>(std::sqrt(last))};
    std::list<Ull> nextLayerVertices {last};

    for (Ull times {0}; times < 4; ++times) {
       nextLayerVertices.emplace_back(nextLayerVertices.back() + (side+1));
    }

    nextLayerVertices.pop_front();
    return nextLayerVertices;
}


[[nodiscard]]
double this_square_ratio(size_t layers,
                         std::shared_ptr<std::vector<bool>> const& is_prime)
{
    std::list<Ull> const start {1};
    std::list<Ull> diagonals {next_layer(start)};

    for (size_t layer {2}; layer <= layers; ++layer) {
        auto tailLayer {next_layer(diagonals)};
        diagonals.splice(diagonals.end(), tailLayer);
    }

    auto howManyPrimes {
        std::ranges::count_if(diagonals.cbegin(), diagonals.cend(),
                              [&is_prime](auto const& candidate)
                              {
                                   return is_prime->at(candidate);
                              })
    };

    return static_cast<double>(howManyPrimes) /
           static_cast<double>(1+diagonals.size());
}



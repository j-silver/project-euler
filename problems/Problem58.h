//
// Created by giuseppe on 06/10/2019.
//

#ifndef PROJECTEULER_PROBLEM58_H
#define PROJECTEULER_PROBLEM58_H

#include <list>
#include "../primes.hpp"

[[nodiscard]] std::list<Ull> next_layer(std::list<Ull> const&);

[[nodiscard]]
double this_square_ratio(size_t layers,
                               std::shared_ptr<std::vector<bool>> const& primes);

#endif //PROJECTEULER_PROBLEM58_H

//
// Created by giuseppe on 06/10/2019.
//

#include <gtest/gtest.h>
#include "Problem58.h"


TEST(add_layer, second_layer)
{
    std::list<Ull> const start {1};
    auto const next {next_layer(start)};
    GTEST_ASSERT_EQ(next.size(), 4);

    std::list<Ull> const expectedResult {3, 5, 7, 9};
    EXPECT_EQ(next, expectedResult);
    SUCCEED();
}


TEST(add_layer, fourth_layer)
{
    std::list<Ull> const third {13, 17, 21, 25};
    auto const next {next_layer(third)};
    GTEST_ASSERT_EQ(third.size(), 4);

    std::list<Ull> expectedResult {31, 37, 43, 49};
    EXPECT_EQ(next, expectedResult);
}


TEST(this_square_ratio, test)
{
    auto primes {which_are_primes(1'000)};
    GTEST_CHECK_(! (this_square_ratio(1, primes) < 2.99/5.0));
    GTEST_CHECK_(this_square_ratio(1, primes) < 3.01/5.0);
    GTEST_CHECK_(! (this_square_ratio(2, primes) < 4.99/9.0));
    GTEST_CHECK_(this_square_ratio(2, primes) < 5.01/9.0);
    GTEST_CHECK_(this_square_ratio(3, primes) < .63);
}


TEST(find, find_solution)
{
    Ull maxLayer {1'000};
    auto primes {which_are_primes((2*maxLayer+1)*(2*maxLayer+1))};
    std::cerr << "primes calculated.\n";
    double lastRatio {0.0};
    for (size_t layer {5}; layer < maxLayer; ++layer) {
        if (lastRatio = this_square_ratio(layer, primes); lastRatio < .1) {
            std::cout << "\nresult: " << 2*layer + 1 << '\n';
            break;
        }
    }
    std::cout << "last ratio was: " << lastRatio << '\n';
}


int main()
{
    ::testing::InitGoogleTest();
    return(RUN_ALL_TESTS());
}

//
// Created by giuseppe on 06/10/2019.
//

#include "Problem59.h"
#include <iostream>

std::unique_ptr<Node<char>> get_dictionary(std::filesystem::path const& path)
{
    std::ifstream inputFile;
    if (std::filesystem::exists(path))
        inputFile.open(path, std::ios_base::in);
    else
        throw std::invalid_argument{"Can't open dictionary"};

    auto root {Node<char>::create_root()};
    std::clog << "Loading dictionary...\n";
    for (std::string word; ! inputFile.eof(); std::getline(inputFile, word)) {
        if (word.length() <= 1)
            continue;
        root->add_word(word);
    }

    return root;
}


std::basic_string<int> read_encrypted_text(std::filesystem::path const& path)
{
    std::ifstream inputFile;
    if (std::filesystem::exists(path)) 
        inputFile.open(path, std::ios_base::in);
    else
        throw std::invalid_argument{"Can't open file"};

    std::basic_string<int> text;
    char comma;
    for (int c; ! inputFile.eof(); inputFile >> comma)
    {
        inputFile >> c;
        text += c; 
    }

    return text;
}


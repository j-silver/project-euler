//
// Created by giuseppe on 06/10/2019.
//

#ifndef PROJECTEULER_PROBLEM59_H
#define PROJECTEULER_PROBLEM59_H

#include "../trie.h"
#include <filesystem>
#include <fstream>

std::unique_ptr<Node<char>> get_dictionary(std::filesystem::path const& path);

std::basic_string<int> read_encrypted_text(std::filesystem::path const& path);

template<typename C>
class Text {
    std::basic_string<C> _text;
public:
    explicit Text(std::basic_string<C> const& s) : _text {s} {}
    std::basic_string<C> decode_with_key(std::string const& key);

    size_t size() const { return _text.size(); }

    template<typename D>
    friend std::ostream& operator<<(std::ostream& os, Text<D> const& t);

    using char_type = C;
};


template<typename C>
std::ostream& operator<<(std::ostream& os, Text<C> const& t)
{
    for (auto const& c : t._text)
        os << c;
    return os;
}


template<typename C>
std::basic_string<C> Text<C>::decode_with_key(std::string const& key)
{
    std::basic_string<C> decoded;
    size_t k {0};
    for (auto&& c : _text) {
        decoded += c ^ key[k%key.length()];
        ++k;
    }

    return decoded;
}


#endif //PROJECTEULER_PROBLEM59_H

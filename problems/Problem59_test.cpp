//
// Created by giuseppe on 06/10/2019.
//

#include <gtest/gtest.h>
#include "Problem59.h"
#include <numeric>


class Fixture : public ::testing::Test {
public:
    std::unique_ptr<Node<char>> _dictionaryRoot;
    std::filesystem::path _dictionaryPath {"/usr/share/dict/words"};

    Fixture() : _dictionaryRoot {} {}
};


TEST(decode, decode_with_key)
{
    std::string nonCoded {"hello world"};
    std::string key {"xyz"};

    std::string encoded;
    for (auto c {nonCoded.begin()}; c != nonCoded.end(); ++c)
        encoded += std::char_traits<char>::to_char_type(
                *c ^ key[static_cast<size_t>(std::distance(nonCoded.begin(), c))%key.size()]
         );

    Text<char> t {encoded};
    auto decoded {t.decode_with_key(key)};

    GTEST_ASSERT_EQ(decoded, nonCoded);
}


TEST_F(Fixture, words)
{
    EXPECT_NO_THROW(_dictionaryRoot = get_dictionary(_dictionaryPath));
    GTEST_CHECK_(_dictionaryRoot->is_root());
    GTEST_CHECK_(_dictionaryRoot->search_word("Giuseppe"));
}



TEST(read_text, text)
{
//    std::filesystem::path textPath {"../problems/p059_cipher.txt"};
    std::filesystem::path textPath {"../problems/gambit.txt"};
    std::basic_string<int> text;
    EXPECT_NO_THROW(text = read_encrypted_text(textPath));
}


TEST_F(Fixture, finding_words)
{
    std::vector<std::string> keys;
    keys.reserve(26*26*26);

    for (char c1 {'a'}; c1 <= 'z'; ++c1) {
        for (char c2 {'a'}; c2 <= 'z'; ++c2) {
            for (char c3 {'a'}; c3 <= 'z'; ++c3) {
                keys.emplace_back(std::string{c1, c2, c3});
            }
        }
    }

    std::clog << "keys generated\n";

    _dictionaryRoot = get_dictionary(_dictionaryPath);
//    std::filesystem::path textPath {"../problems/p059_cipher.txt"};
    std::filesystem::path textPath {"../problems/gambit.txt"};
    auto encodedText {read_encrypted_text(textPath)};
    std::string encoded;
    for (auto const& c : encodedText)
        encoded += static_cast<char>(c);

    Text<char> t {encoded};
    std::clog << "text and dictionary read\n";
    std::clog << "printing all the decoded words longer than 7 characters: \n";

    for (auto const& key : keys) {
        auto decoded {t.decode_with_key(key)};
        std::string word;
        std::istringstream decodedStream {decoded};
        while (decodedStream >> word) {
            if (word.length() > 7 && _dictionaryRoot->search_word(word)) {
                std::clog << "key: " << key << '\n';
                std::clog << "decoded word: " << word << '\n';
            }
        }
    }

    SUCCEED();
}

TEST_F(Fixture, solution)
{
    std::string key {"exp"};
    _dictionaryRoot = get_dictionary(_dictionaryPath);
//    std::filesystem::path textPath {"../problems/p059_cipher.txt"};
    std::filesystem::path textPath {"../problems/gambit.txt"};
    auto encodedText {read_encrypted_text(textPath)};
    std::string encoded;
    for (auto const& c : encodedText)
        encoded += static_cast<char>(c);

    Text<char> t {encoded};
    std::clog << "text and dictionary read\n";
    auto decoded {t.decode_with_key(key)};
    std::string word;
    std::istringstream decodedStream {decoded};
    int sum {0};
    while (decodedStream >> word) {
        std::clog << word << ' ';
        int init {0};
        sum += std::accumulate(cbegin(word), cend(word), init,
                               [](auto s, auto c)
                               { return s + static_cast<int>(c); });
        sum += static_cast<int>(' ');
    }

    sum -= static_cast<int>(' ');
    std::clog << '\n';

    std::cout << "Sum of ascii values: " << sum << '\n';
    SUCCEED();
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

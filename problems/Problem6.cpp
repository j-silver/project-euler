//
// Created by giuseppe on 20/11/2020.
//

#include <iostream>
#include <vector>
#include <numeric>
#include <cassert>
#include <cstdint>

uint64_t sum_of_double_product(std::vector<uint64_t> const& numbers)
{
    std::vector<uint64_t> sums;
    sums.reserve(numbers.size());
    for (auto iter {numbers.begin()}; iter != numbers.end(); ++iter) {
        auto const sumOfProducts {
            std::accumulate(std::next(iter), numbers.end(),
                                  uint64_t {0},
                                  [a=*iter](auto init, auto b){ return init += 2*a*b; })
        };
        sums.push_back(sumOfProducts);
    }

    return std::accumulate(sums.cbegin(), sums.cend(), uint64_t {0});
}

int main()
{
    std::vector<uint64_t> two {1, 2, 3};
    std::cout << sum_of_double_product(two) << '\n';
    std::vector<uint64_t> ten (10, 0);
    std::iota(ten.begin(), ten.end(), uint64_t {1});
    std::cout << sum_of_double_product(ten) << '\n';
    std::vector<uint64_t> oneHundred (100, 0);
    std::iota(oneHundred.begin(), oneHundred.end(), uint64_t {1});
    auto const answer {sum_of_double_product(oneHundred)};
    assert(answer == 25164150);
    std::cout << answer << '\n';
}

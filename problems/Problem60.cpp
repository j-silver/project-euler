#include <set>
#include <string>
#include <charconv>
#include <algorithm>
#include <numeric>
#include <iostream>
#include "../primes.hpp"

std::set const four {3, 7, 109, 673};

int concat(int lhs, int rhs)
{
    auto const concat {std::to_string(lhs) + std::to_string(rhs)};
    int result;
    std::from_chars(concat.c_str(), concat.c_str() + concat.size(), result);
    return result;
}

bool check(std::set<int> const& concatenatable, int n, std::shared_ptr<std::vector<Ull> const> const& primes)
{
    return std::ranges::all_of(concatenatable,
                       [n, &primes](auto const& num) {
                           return is_prime(concat(num, n), primes) && is_prime(concat(n, num), primes);
                       });
}

int sum(std::set<int> const& concatenatable)
{
    return std::reduce(concatenatable.cbegin(), concatenatable.cend(), 0);
}

int main()
{
    auto const primes {find_primes_up_to(1'000'000)};
    std::cout << std::boolalpha << check({3, 7, 109}, 673, primes) << '\n';
}
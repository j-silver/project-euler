//
// Created by giuseppe on 21/11/2020.
//

#include "../primes.hpp"
#include <cmath>
#include <iostream>
#include <cassert>

using sz_type = std::vector<Ull>::size_type;

constexpr sz_type position {10'001};

int main()
{
    sz_type length {position};
    while (static_cast<sz_type>(static_cast<double>(length)/log(length)) <= position)
    {
        ++length;
    }
    auto const primes {find_primes_up_to(length)};
    if (primes->size() < position) {
        throw std::length_error("primes vector size is " + std::to_string(primes->size()));
    }

    auto const answer {primes->operator[](position-1)};
    assert(answer == 104743);
    std::cout << answer << '\n';
}
// Problem71
//
// by Giuseppe Argentieri

#include <iostream>
#include <iomanip>
#include <ranges>
#include <limits>
#include <algorithm>


using Ull = unsigned long long;

class Rational {
    Ull _n;
    Ull _d;
public:
    explicit Rational (Ull n = 1, Ull d = 1) : _n {n}, _d {d}
    {
        if (_d == 0)
            throw std::invalid_argument{"Cannot use 0 as denominator"};
    }

    friend bool operator<(Rational const& r1, Rational const& r2)
    {
        return r1._n * r2._d < r1._d * r2._n;
    }

    [[nodiscard]] Ull n() const { return _n; }
    [[nodiscard]] Ull d() const { return _d; }
 };


constexpr Ull maximumDenominator {1'000'000};

int main()
{
    std::cerr << std::numeric_limits<Ull>::digits10 << '\n';
    std::cerr << std::numeric_limits<Ull>::max() << '\n';
    std::cerr << "3/7 = " << std::setprecision(10) << 3.0/7.0 << '\n';

    Rational closest {0};

    // implementation with ranges
    for (auto const& d : std::ranges::views::iota(1ull, maximumDenominator)) {
        auto const rationals {
            std::ranges::views::take_while(std::ranges::iota_view(closest.n()),
                              [&d](auto const& n){ return n*7 < 3*d; })
            | std::ranges::views::transform([&d](auto const& n){ return Rational {n, d}; })
        };

        closest = std::max(std::ranges::max(rationals, std::less<Rational>{}), closest);
    }

    std::cout << closest.n() << '/' << closest.d() <<  " = "
        << std::setprecision(10)
        << static_cast<double>(closest.n())/static_cast<double>(closest.d())
        << '\n';
}

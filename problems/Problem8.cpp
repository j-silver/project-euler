//
// Created by giuseppe on 21/11/2020.
//
#include <iostream>
#include <fstream>
#include <ranges>
#include <filesystem>
#include <numeric>
#include <cassert>

using string_range_sz = std::ranges::range_difference_t<std::string>;

int main()
{
    std::filesystem::path const filePath {"../problems/Problem8.txt"};
    std::ifstream fileStream {filePath};
    std::string number;
    number.reserve(1'000);
    std::copy(std::istream_iterator<char>(fileStream),
            std::istream_iterator<char>{},
            std::back_inserter(number));
    auto currentMax {1ULL};
    for (string_range_sz pos {0}; pos < static_cast<string_range_sz>(number.size() - 13); ++pos) {
        auto window {
            std::ranges::drop_view{std::string_view {number}, pos}
            | std::ranges::views::take(13)
        };
        auto product {
            std::accumulate(std::cbegin(window), std::cend(window), 1ULL,
                            [](auto const& init, auto const& factor)
                            { return init*(factor - '0'); })
        };
        currentMax = std::max(currentMax, product);
    }
    assert(currentMax == 23514624000);
    std::cout << currentMax << '\n';
}

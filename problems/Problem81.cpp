//
// Created by giuseppe on 13/12/2019.
//

#include "Problem81.h"

#include <utility>
#include <algorithm>
#include <fstream>
#include <map>
#include <iterator>
#include <ranges>
#include <iostream>

Node::Node(int value)
        : _value {value}
{}

Node*& Node::right()
{
    return _right;
}

Node*& Node::left()
{
    return _left;
}

void Node::add_left(Node* left)
{
    if (_left != nullptr) {
        throw std::invalid_argument {"Node already exists"};
    }

    _left = left;
}

void Node::add_right(Node* right)
{
    if (_right != nullptr) {
        throw std::invalid_argument {"Node already exists"};
    }

    _right = right;
}

int Node::value() const
{
    return _value;
}

Matrix::Matrix(std::vector<std::vector<int>> matrix)
        : _matrix(std::move(matrix)), _rank {_matrix.size()}
{
    if (std::ranges::any_of(_matrix,
                            [this](auto const& row) { return row.size() != _rank; })) {
        throw std::invalid_argument {"Matrix is not square"};
    }
}

std::optional<int> Matrix::value(size_t i, size_t j) const
{
    if (i <= _rank - 1 && j <= _rank - 1) {
        return std::make_optional<int>(_matrix.at(i).at(j));
    }

    return std::nullopt;
}

size_t Matrix::rank() const
{
    return _matrix.size();
}

Matrix::Matrix() : _rank {}
{}

Root::Root(int value)
        : Node {value}
{
    _visited[{0, 0}] = std::make_unique<Node>(this->value());
}

Node* Root::get() const
{
    return _visited.at({0, 0}).get();
}

void Root::create_graph(Matrix const& matrix)
{
    auto rank {matrix.rank()};

    for (size_t i {0}; i < rank; ++i) {
        for (size_t j {0}; j < rank; ++j) {
            if (i != 0 || j != 0) {
                _visited[{i, j}] = std::make_unique<Node>(matrix.value(i, j).value());
            }
        }
    }

    for (size_t i {0}; i < rank; ++i) {
        for (size_t j {0}; j < rank; ++j) {
            if (i + 1 < rank) {
                _visited[{i, j}]->add_left(_visited[{i + 1, j}].get());
            }
            if (j + 1 < rank) {
                _visited[{i, j}]->add_right(_visited[{i, j + 1}].get());
            }
        }
    }
}

std::vector<std::vector<int>> read_file(std::filesystem::path const& filePath)
{
    std::ifstream fileStream {filePath};
    std::vector<std::vector<int>> matrix;

    std::string row;
    while (std::getline(fileStream, row)) {
        std::vector<int> rowOfNumbers;
        std::ranges::split_view numbers {row, ','};
        std::ranges::transform(numbers, std::back_inserter(rowOfNumbers),
                               [](auto const& num){ return std::stoi(std::string{num.begin(), num.end()}); });
        matrix.push_back(rowOfNumbers);
    }

    return matrix;
}

Node* reorder(Node* fromThisNode)
{
    auto* itsLeft {fromThisNode->left()};
    auto* itsRight {fromThisNode->right()};

    if (itsLeft != nullptr) {
        reorder(itsLeft);
    }

    if (itsLeft != nullptr && itsRight != nullptr && itsLeft->value() > itsRight->value()) {
        std::swap(itsLeft, itsRight);
        auto* leftLeft {itsLeft->left()};
        itsLeft->left() = itsLeft->right();
        itsLeft->right() = leftLeft;
        std::swap(itsRight->left(), itsRight->right());
    }

    if (itsRight != nullptr) {
        reorder(itsRight);
    }

    return fromThisNode;
}


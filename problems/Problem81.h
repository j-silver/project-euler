//
// Created by giuseppe on 13/12/2019.
//

#ifndef PROJECTEULER_PROBLEM81_H
#define PROJECTEULER_PROBLEM81_H

#include <vector>
#include <memory>
#include <filesystem>
#include <optional>
#include <map>


class Matrix;

class Node {
public:
    explicit Node(int value);

    Node(Node const&) = delete;
    Node& operator=(Node const&) = delete;
    Node(Node&&) = default;
    Node& operator=(Node&&) = default;

    [[nodiscard]] auto value() const -> int;
    [[nodiscard]] auto left() -> Node*& ;
    [[nodiscard]] auto right() -> Node*&;

    void add_left(Node* left);
    void add_right(Node* right);

    virtual ~Node() = default;

private:
    int _value;
    Node* _left {nullptr};
    Node* _right {nullptr};
};


class Root : public Node {
    std::map<std::pair<size_t, size_t>, std::unique_ptr<Node>> _visited {};

public:
    explicit Root(int value = 0);
    void create_graph(Matrix const& matrix);

    [[nodiscard]] auto get() const -> Node*;
};


class Matrix {
public:
    Matrix();
    explicit Matrix(std::vector<std::vector<int>> matrix);

    [[nodiscard]] std::optional<int> value(size_t i, size_t j) const;
    [[nodiscard]] size_t rank() const;

private:
    std::vector<std::vector<int>> _matrix {};
    size_t _rank;
};

Node* reorder(Node* fromThisNode);

[[nodiscard]] Node* create_graph(Matrix& matrix);

std::vector<std::vector<int>> read_file(std::filesystem::path const& filePath);

#endif //PROJECTEULER_PROBLEM81_H

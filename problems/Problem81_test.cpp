//
// Created by giuseppe on 13/12/2019.
//

#include <iostream>
#include "Problem81.h"
#include <gtest/gtest.h>

class MatrixFixture : public ::testing::Test {

    const std::filesystem::path _matrixFile {"../problems/p081_matrix.txt"};
    const Matrix _matrix;
    Root _root {};

public:
    explicit MatrixFixture()
            : _matrix {read_file(_matrixFile)},
              _root {m().value(0, 0).value()}
    {
    }

    [[nodiscard]] Matrix const& m() const
    { return _matrix; }

    [[nodiscard]] Root& get_root()
    { return _root; }
};

TEST_F(MatrixFixture, load_matrix)
{
    std::cerr << "Print Matrix\n";
    auto rankOfMatrix {m().rank()};
    for (size_t i {0}; i < rankOfMatrix; ++i) {
        for (size_t j {0}; j < rankOfMatrix; ++j) {
            std::cerr << m().value(i, j).value() << '\t';
        }
        std::cerr << '\n';
    }
    GTEST_SUCCEED();
}

TEST_F(MatrixFixture, create_graph)
{
    std::cerr << "Creating nodes...\n";
    get_root().create_graph(m());
    GTEST_SUCCEED();
}

TEST_F(MatrixFixture, go_left)
{
    std::cerr << "go_left\n";
    get_root().create_graph(m());
    std::cerr << get_root().value() << ' ';
    for (auto* l {get_root().get()->left()}; l != nullptr; l = l->left()) {
        std::cerr << l->value() << ' ';
    }

    std::cerr << '\n';
    GTEST_SUCCEED();
}

TEST_F(MatrixFixture, go_right)
{
    std::cerr << "go_right\n";
    Root root {m().value(0, 0).value()};
    root.create_graph(m());
    std::cerr << root.value() << ' ';
    for (auto* r {root.get()->right()}; r != nullptr; r = r->right()) {
        std::cerr << r->value() << ' ';
    }

    std::cerr << '\n';
    GTEST_SUCCEED();
}

TEST_F(MatrixFixture, zig_zag)
{
    std::cerr << "ZigZag\n";
    std::cerr << get_root().value() << '\t';
    get_root().create_graph(m());
    bool nextLeft {true};
    for (auto* nextNode {get_root().get()->left()};
         nextNode != nullptr;
         nextNode = (nextLeft ? nextNode->left() : nextNode->right()))
    {
        std::cerr << nextNode->value() << '\t';
        nextLeft = !nextLeft;
    }

    std::cerr << '\n';

    std::cerr << get_root().value() << '\t';
    nextLeft = false;
    for (auto* nextNode {get_root().get()->right()};
         nextNode != nullptr;
         nextNode = (nextLeft ? nextNode->left() : nextNode->right()))
    {
        std::cerr << nextNode->value() << '\t';
        nextLeft = !nextLeft;
    }

    std::cerr << '\n';
    GTEST_SUCCEED();
}

TEST_F(MatrixFixture, diagonal)
{
    std::cerr << "Diagonal\n";
    Root& r {get_root()};
    r.create_graph(m());

    for (Node* n {r.get()}; n != nullptr;) {
        std::cerr << n->value() << '\t';
        if (n->left() == nullptr)
            break;
        n = n->left();
        if (n->right() == nullptr)
            break;
        n = n->right();
    }

    std::cerr << '\n';
    GTEST_SUCCEED();
}

TEST_F(MatrixFixture, DISABLED_reorder)
{
    std::cerr << "Reorder\n";
    get_root().create_graph(m());
    auto& r {get_root()};
    auto newRoot {reorder(r.get())};

    std::cerr << "Going left...\n";
    for (auto n {newRoot}; n != nullptr; n = n->left())
        std::cerr << n->value() << '\t';
    std::cerr << '\n';

    std::cerr << "Going right...\n";
    for (auto n {newRoot}; n != nullptr; n = n->right())
        std::cerr << n->value() << '\t';

    std::cerr << '\n';
    GTEST_SUCCEED();
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

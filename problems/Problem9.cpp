//
// Created by giuseppe on 21/11/2020.
//

#include <iostream>
#include <ranges>
#include <cassert>

using namespace std::ranges;

int main()
{
    for (std::weakly_incrementable auto c : views::iota(3, 998)) {
        for (std::weakly_incrementable auto b : views::iota(2, c)) {
            for (std::weakly_incrementable auto a : views::iota(1, b)) {
                if (a + b + c != 1'000) {
                    continue;
                }
                if (a*a + b*b == c*c) {
                    assert(a*b*c == 31875000);
                    std::cout << a*b*c << '\n';
                    exit(0);
                }
            }
        }
    }
}

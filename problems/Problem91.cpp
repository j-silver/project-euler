//
// Created by giuseppe on 01/10/2019.
//
#include <cmath>
#include <iostream>


constexpr int Max {50};


bool are_right(int x1, int y1, int x2, int y2)
{
    if (x1 == x2 && y1 == y2) {
        return false;
    }

    auto r1 {x1*x1 + y1*y1};
    auto r2 {x2*x2 + y2*y2};
    auto r12 {(x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)};

    if (r1 == 0 || r2 == 0) {
        return false;
    }
    return (r1 + r2 == r12) || (r1 == r2 + r12) || (r1 + r12 == r2);
}


int main()
{
    int count {0};

    for (int x1 {0}; x1 <= Max; ++x1) {
        for (int y1 {0}; y1 <= Max; ++y1) {
            for (int x2 {0}; x2 <= Max; ++x2) {
                for (int y2 {0}; y2 <= Max; ++y2) {
                    if (are_right(x1, y1, x2, y2)) {
                        ++count;
                    }
                }
            }
        }
    }

    count /= 2;

    std::cout << count << '\n';
}
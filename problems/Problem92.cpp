#include <string>
#include <set>
#include <iostream>
#include <charconv>
#include <numeric>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <ranges>

constexpr unsigned limit {10'000'000};


class arrive_at_89 {
private:
	std::set<unsigned> arrived;
public:
	arrive_at_89() : arrived {89} {}
	bool check(unsigned n);
	[[nodiscard]] auto how_many() const { return arrived.size(); }
	friend std::ostream& operator<<(std::ostream& os, arrive_at_89 const& a);
};


bool arrive_at_89::check(unsigned n)
{
	if (n == 89 || arrived.find(n) != arrived.end())
		return true;
	else if (n == 1)
		return false;

	auto const number {std::to_string(n)};
    auto sum_squares {
        [](unsigned temporarySum, auto const& digit)
        {
            return temporarySum + 
                   std::pow(static_cast<unsigned>(digit-'0'), 2);
        }
    };

    auto sumOfSquares {std::accumulate(number.cbegin(), number.cend(), 0, sum_squares)};

	if (check(sumOfSquares)) {
		arrived.insert(n);
		return true;
	}

	return false;
}


std::ostream& operator<<(std::ostream& os, arrive_at_89 const& a)
{
	for (auto const& n : a.arrived)
		os << n << ' ';

	return os << '\n';
}


int main()
{
	arrive_at_89 N;
    std::ranges::for_each(std::ranges::views::iota(1u, limit),
                    [&N](auto const& i){ N.check(i); });

	std::cout << N.how_many() << std::endl;
}

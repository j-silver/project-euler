//
// Created by giuseppe on 10/09/2019.
//

#include <cmath>
#include <fstream>
#include <iostream>
#include <cassert>


class exponential {
    double base;
    unsigned exponent;
public:
    exponential(double b, unsigned e) : base {b}, exponent {e} {}

    friend bool operator<(exponential const& E1, exponential const& E2);
};


double reduce(double x, double a, unsigned n, unsigned m)
{
    return std::pow(x, m)*std::pow(a, n-m);
}


bool operator<(exponential const& E1, exponential const& E2)
{
    if ((E1.base < E2.base && E1.exponent <= E2.exponent) ||
        (E1.base == E2.base && E1.exponent < E2.exponent))
        return true;
    else if (E1.base >= E2.base && E1.exponent >= E2.exponent)
        return false;
    else
        return E1.exponent*log(E1.base) < E2.exponent*log(E2.base);
}


int main()
{
    double a {0};
    unsigned b {0};
    char separator;
    exponential maximum {0, 1};
    unsigned line {0};
    unsigned count {1};

    for (std::cin >> a >> separator >> b;
         std::cin.good();
         std::cin >> a >> separator >> b)
    {
        if (maximum < exponential {a, b}) {
            line = count;
            maximum = exponential {a, b};
            std::cerr << a << ' ' << b << '\n';
        }
        ++count;
    }

    assert(count == 1'000);
    std::cout << line;
}

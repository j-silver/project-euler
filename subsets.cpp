//
// Created by giuseppe on 06/05/19.
//

#include "subsets.h"
#include "choose.h"


template<std::ranges::range Rng>
[[nodiscard]]
Subsets<typename Rng::const_iterator> subsets_from_range(Rng const& rng)
{
    if (std::distance(cbegin(rng), cend(rng)) < 0) {
        throw std::invalid_argument("Invalid _start/end iterators");
    }

    return Subsets {cbegin(rng), cend(rng)};
}


template<std::forward_iterator Fwd>
Subsets<Fwd>::Subsets(Fwd start, Fwd end)
    : _start {start}, _end {end}, _size {0}
{
    auto dist {std::distance(_start, _end)};
    if (dist < 0) {
        throw std::invalid_argument("Invalid _start/end iterators");
    }
    _size = static_cast<size_t>(dist);
}


template<std::forward_iterator Fwd>
[[nodiscard]]
std::vector<std::vector<Fwd>> Subsets<Fwd>::build(size_t k) const
{
    if (k > _size) {
        throw std::invalid_argument {"Cannot create subsets of size k larger than initial set"};
    }

    std::vector<std::vector<Fwd>> nextGroup;
    nextGroup.reserve(choose(_size, k));

    if (k == 1) {
        for (auto it {_start}; it != _end; ++it) {
            nextGroup.push_back(std::vector<Fwd> {it});
        }
        return nextGroup;
    }

    for (auto& subNMinusOne : build(k - 1)) {
        for (auto it {std::next(subNMinusOne.back())}; it != _end; ++it) {
            auto copyOfSub {subNMinusOne};
            copyOfSub.push_back(it);
            nextGroup.push_back(copyOfSub);
        }
    }

    return nextGroup;
}


template<std::forward_iterator Fwd>
template<std::ranges::range Rng>
Subsets<Fwd>::Subsets(Rng const& rng)
    : _start {cbegin(rng)}, _end {cend(rng)}, _size {}
{
    auto dist {std::distance(cbegin(rng), cend(rng))};

    if (dist < 0) {
        throw std::invalid_argument("Invalid _start/end iterators");
    }
    _size  = static_cast<size_t>(dist);
}



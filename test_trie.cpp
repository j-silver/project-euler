#include "trie.h"
#include <iostream>
#include <filesystem>
#include <fstream>
#include <algorithm>
#include <cctype>

int main(int argc, char* argv[])
{
    std::string inputWord {"Giuseppe"};
    if (argc > 1)
        inputWord = argv[1];

    std::filesystem::path wordsPath {"/usr/share/dict/words"};
    std::ifstream fileStream {wordsPath};
    auto root {Node<char>::create_root()};

    std::clog << "Loading dictionary...\n";
    for (std::string word;
         ! fileStream.eof();
         std::getline(fileStream, word))
    {
        if (word.length() <= 1)
            continue;
        root->add_word(word);
    }

    std::cout << "Is '" << inputWord << "' in dictionary: " << std::boolalpha
        << root->search_word(inputWord) << '\n';
}

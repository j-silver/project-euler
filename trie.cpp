#include "trie.h"


template<>
[[nodiscard]] std::unique_ptr<Node<char>> Node<char>::create_root()
{
    auto root {std::make_unique<Node<char>>()};
    for (char c {' '}; c < 127; ++c)
        root->_children.emplace(c, std::make_unique<Node<char>>());

    root->_isRoot = true;

    return root;
}




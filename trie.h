#ifndef TRIE_H
#define TRIE_H

#include <memory>
#include <map>

template<typename C>
class Node {
    C                                     _letter;
    std::map<C, std::unique_ptr<Node>>    _children;
    bool                                  _last {false};
    bool                                  _isRoot {false};

    Node* operator[](C nextLetter);

public:
    [[nodiscard]] static std::unique_ptr<Node> create_root();
    Node() : _letter {}, _children {} {}
    explicit Node(C l) : _letter {l}, _children {} {}

    Node(Node const&) = delete;
    Node(Node&&) = delete;
    Node& operator=(Node const&) = delete;
    Node& operator=(Node&&) noexcept = delete;

    void add_word(std::basic_string<C> const& word);
    bool search_word(std::basic_string<C> const& word);

    [[nodiscard]] bool is_root() const { return _isRoot; }
};


template<typename C>
Node<C>* Node<C>::operator[](C nextLetter)
{
    return this->_children[nextLetter].get();
}


template<typename C>
void Node<C>::add_word(std::basic_string<C> const& word)
{
    if (!_isRoot && word[0] != _letter)
        throw std::invalid_argument{"Wrong starting letter"};

    auto start {this};

    if (_isRoot) {
        auto emplace {
            this->_children.try_emplace(_letter, std::make_unique<Node>())
        };
        start = emplace.first->second.get();
    }


    auto const w {word.substr(1)};
    for (auto const& letter : w) {
        start->_children.emplace(letter, std::make_unique<Node>(letter));
        start = start->operator[](letter);
    }

    start->_last = true;
}


template<typename C>
bool Node<C>::search_word(std::basic_string<C> const& word)
{
    if (!_isRoot && word[0] != _letter)
        throw std::invalid_argument{"invalid starting letter"};
    auto start {this};
    if (_isRoot)
        start = this->operator[](_letter);

    auto const w {word.substr(1)};
    for (auto const& letter : w) {
        if (start->_children.find(letter) == start->_children.end())
            return false;
        start = start->operator[](letter);
    }

    return start->_last;
}




#endif // TRIE_H
